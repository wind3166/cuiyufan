-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- 主机: 127.0.0.1
-- 生成日期: 2015 �?07 �?10 �?09:40
-- 服务器版本: 5.6.11
-- PHP 版本: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;



-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_action`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '行為唯壹標識',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '行為說明',
  `remark` char(140) NOT NULL DEFAULT '' COMMENT '行為描述',
  `rule` text NOT NULL COMMENT '行為規則',
  `log` text NOT NULL COMMENT '日誌規則',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '類型',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系統行為表' AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `cuiyufang_action`
--

INSERT INTO `cuiyufang_action` (`id`, `name`, `title`, `remark`, `rule`, `log`, `type`, `status`, `update_time`) VALUES
(1, 'user_login', '用戶登錄', '積分+10，每天壹次', 'table:member|field:score|condition:uid={$self} AND status>-1|rule:score+10|cycle:24|max:1;', '[user|get_nickname]在[time|time_format]登錄了後臺', 1, 1, 1387181220),
(2, 'add_article', '發布文章', '積分+5，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:5', '', 2, 0, 1380173180),
(3, 'review', '評論', '評論積分+1，無限制', 'table:member|field:score|condition:uid={$self}|rule:score+1', '', 2, 1, 1383285646),
(4, 'add_document', '發表文檔', '積分+10，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+10|cycle:24|max:5', '[user|get_nickname]在[time|time_format]發表了壹篇文章。\r\n表[model]，記錄編號[record]。', 2, 0, 1386139726),
(5, 'add_document_topic', '發表討論', '積分+5，每天上限10次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:10', '', 2, 0, 1383285551),
(6, 'update_config', '更新配置', '新增或修改或刪除配置', '', '', 1, 1, 1383294988),
(7, 'update_model', '更新模型', '新增或修改模型', '', '', 1, 1, 1383295057),
(8, 'update_attribute', '更新屬性', '新增或更新或刪除屬性', '', '', 1, 1, 1383295963),
(9, 'update_channel', '更新導航', '新增或修改或刪除導航', '', '', 1, 1, 1383296301),
(10, 'update_menu', '更新菜單', '新增或修改或刪除菜單', '', '', 1, 1, 1383296392),
(11, 'update_category', '更新分類', '新增或修改或刪除分類', '', '', 1, 1, 1383296765);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_action_log`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行為id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '執行用戶id',
  `action_ip` bigint(20) NOT NULL COMMENT '執行行為者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '觸發行為的表',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '觸發行為的數據id',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '日誌備註',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '執行行為的時間',
  PRIMARY KEY (`id`),
  KEY `action_ip_ix` (`action_ip`),
  KEY `action_id_ix` (`action_id`),
  KEY `user_id_ix` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行為日誌表' AUTO_INCREMENT=193 ;

--
-- 转存表中的数据 `cuiyufang_action_log`
--

INSERT INTO `cuiyufang_action_log` (`id`, `action_id`, `user_id`, `action_ip`, `model`, `record_id`, `remark`, `status`, `create_time`) VALUES
(1, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-05 11:35登錄了後臺', 1, 1433475321),
(2, 7, 1, 2130706433, 'model', 4, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433475391),
(3, 8, 1, 2130706433, 'attribute', 33, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433475448),
(4, 7, 1, 2130706433, 'model', 4, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433475479),
(5, 7, 1, 2130706433, 'model', 4, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433475519),
(6, 7, 1, 2130706433, 'model', 2, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476364),
(7, 7, 1, 2130706433, 'model', 1, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476401),
(8, 7, 1, 2130706433, 'model', 5, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476425),
(9, 8, 1, 2130706433, 'attribute', 34, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476460),
(10, 8, 1, 2130706433, 'attribute', 35, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476485),
(11, 7, 1, 2130706433, 'model', 5, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476507),
(12, 7, 1, 2130706433, 'model', 5, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476534),
(13, 7, 1, 2130706433, 'model', 6, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476566),
(14, 8, 1, 2130706433, 'attribute', 36, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476597),
(15, 8, 1, 2130706433, 'attribute', 37, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476628),
(16, 8, 1, 2130706433, 'attribute', 38, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476680),
(17, 8, 1, 2130706433, 'attribute', 39, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476713),
(18, 8, 1, 2130706433, 'attribute', 40, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476732),
(19, 7, 1, 2130706433, 'model', 6, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476750),
(20, 7, 1, 2130706433, 'model', 6, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476783),
(21, 7, 1, 2130706433, 'model', 7, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433476818),
(22, 8, 1, 2130706433, 'attribute', 41, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476841),
(23, 8, 1, 2130706433, 'attribute', 42, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476858),
(24, 8, 1, 2130706433, 'attribute', 43, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476876),
(25, 8, 1, 2130706433, 'attribute', 44, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476890),
(26, 8, 1, 2130706433, 'attribute', 45, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433476922),
(27, 7, 1, 2130706433, 'model', 7, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477203),
(28, 7, 1, 2130706433, 'model', 7, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477239),
(29, 7, 1, 2130706433, 'model', 7, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477279),
(30, 7, 1, 2130706433, 'model', 8, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477327),
(31, 8, 1, 2130706433, 'attribute', 46, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477357),
(32, 8, 1, 2130706433, 'attribute', 47, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477370),
(33, 8, 1, 2130706433, 'attribute', 48, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477389),
(34, 8, 1, 2130706433, 'attribute', 49, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477412),
(35, 8, 1, 2130706433, 'attribute', 50, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477449),
(36, 8, 1, 2130706433, 'attribute', 51, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477470),
(37, 7, 1, 2130706433, 'model', 9, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477490),
(38, 8, 1, 2130706433, 'attribute', 52, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477510),
(39, 7, 1, 2130706433, 'model', 9, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477529),
(40, 7, 1, 2130706433, 'model', 9, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477552),
(41, 7, 1, 2130706433, 'model', 10, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477581),
(42, 8, 1, 2130706433, 'attribute', 53, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477603),
(43, 7, 1, 2130706433, 'model', 10, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477620),
(44, 7, 1, 2130706433, 'model', 10, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477651),
(45, 7, 1, 2130706433, 'model', 11, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477669),
(46, 8, 1, 2130706433, 'attribute', 54, '操作url：/wwwroot/index.php?s=/admin/attribute/update.html', 1, 1433477688),
(47, 7, 1, 2130706433, 'model', 11, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477706),
(48, 7, 1, 2130706433, 'model', 11, '操作url：/wwwroot/index.php?s=/admin/model/update.html', 1, 1433477733),
(49, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-05 12:25登錄了後臺', 1, 1433478335),
(50, 11, 1, 2130706433, 'category', 1, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478393),
(51, 11, 1, 2130706433, 'category', 2, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478402),
(52, 11, 1, 2130706433, 'category', 39, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478443),
(53, 11, 1, 2130706433, 'category', 39, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478451),
(54, 11, 1, 2130706433, 'category', 2, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478456),
(55, 11, 1, 2130706433, 'category', 2, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478462),
(56, 11, 1, 2130706433, 'category', 40, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478643),
(57, 11, 1, 2130706433, 'category', 40, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478648),
(58, 11, 1, 2130706433, 'category', 41, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478685),
(59, 11, 1, 2130706433, 'category', 41, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478690),
(60, 11, 1, 2130706433, 'category', 42, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478768),
(61, 11, 1, 2130706433, 'category', 42, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478787),
(62, 11, 1, 2130706433, 'category', 43, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478819),
(63, 11, 1, 2130706433, 'category', 44, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478847),
(64, 11, 1, 2130706433, 'category', 44, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478852),
(65, 11, 1, 2130706433, 'category', 45, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478873),
(66, 11, 1, 2130706433, 'category', 45, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478879),
(67, 11, 1, 2130706433, 'category', 46, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478914),
(68, 11, 1, 2130706433, 'category', 46, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433478926),
(69, 11, 1, 2130706433, 'category', 47, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433478979),
(70, 11, 1, 2130706433, 'category', 48, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1433479028),
(71, 11, 1, 2130706433, 'category', 47, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1433479032),
(72, 8, 1, 2130706433, 'attribute', 33, '操作url：/cuiyufan/index.php?s=/Admin/Attribute/update.html', 1, 1433482916),
(73, 8, 1, 2130706433, 'attribute', 33, '操作url：/cuiyufan/index.php?s=/Admin/Attribute/update.html', 1, 1433482935),
(74, 11, 1, 2130706433, 'category', 47, '操作url：/cuiyufan/index.php?s=/Admin/Category/edit.html', 1, 1433483077),
(75, 11, 1, 2130706433, 'category', 43, '操作url：/cuiyufan/index.php?s=/Admin/Category/edit.html', 1, 1433483477),
(76, 9, 1, 2130706433, 'channel', 2, '操作url：/cuiyufan/index.php?s=/Admin/Channel/edit.html', 1, 1433484035),
(77, 9, 1, 2130706433, 'channel', 3, '操作url：/cuiyufan/index.php?s=/Admin/Channel/edit.html', 1, 1433484091),
(78, 10, 1, 2130706433, 'Menu', 122, '操作url：/cuiyufan/index.php?s=/Admin/Menu/add.html', 1, 1433484298),
(79, 10, 1, 2130706433, 'Menu', 1, '操作url：/cuiyufan/index.php?s=/Admin/Menu/edit.html', 1, 1433484316),
(80, 10, 1, 2130706433, 'Menu', 122, '操作url：/cuiyufan/index.php?s=/Admin/Menu/edit.html', 1, 1433484334),
(81, 10, 1, 2130706433, 'Menu', 122, '操作url：/cuiyufan/index.php?s=/Admin/Menu/edit.html', 1, 1433484352),
(82, 11, 1, 2130706433, 'category', 44, '操作url：/cuiyufan/index.php?s=/Admin/Category/edit.html', 1, 1433485182),
(83, 11, 1, 2130706433, 'category', 45, '操作url：/cuiyufan/index.php?s=/Admin/Category/edit.html', 1, 1433485313),
(84, 6, 1, 2130706433, 'config', 20, '操作url：/cuiyufan/index.php?s=/Admin/Config/edit.html', 1, 1433485817),
(85, 10, 1, 2130706433, 'Menu', 2, '操作url：/cuiyufan/index.php?s=/Admin/Menu/edit.html', 1, 1433486173),
(86, 10, 1, 2130706433, 'Menu', 123, '操作url：/cuiyufan/index.php?s=/Admin/Menu/add.html', 1, 1433486230),
(87, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-05 14:48登錄了後臺', 1, 1433486919),
(88, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-05 14:52登錄了後臺', 1, 1433487135),
(89, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-06 09:33登錄了後臺', 1, 1433554400),
(90, 6, 1, 2130706433, 'config', 9, '操作url：/cuiyufan/Admin/Config/edit.html', 1, 1433555169),
(91, 6, 1, 2130706433, 'config', 42, '操作url：/cuiyufan/Admin/Config/edit.html', 1, 1433555673),
(92, 6, 1, 2130706433, 'config', 45, '操作url：/cuiyufan/Admin/Config/edit.html', 1, 1433555726),
(93, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-06 11:47登錄了後臺', 1, 1433562461),
(94, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-08 10:14登錄了後臺', 1, 1433729650),
(95, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-11 16:38登錄了後臺', 1, 1434011893),
(96, 11, 1, 2130706433, 'category', 49, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1434012030),
(97, 11, 1, 2130706433, 'category', 49, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1434012041),
(98, 11, 1, 2130706433, 'category', 50, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1434012107),
(99, 11, 1, 2130706433, 'category', 51, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1434012194),
(100, 7, 1, 2130706433, 'model', 12, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434012376),
(101, 8, 1, 2130706433, 'attribute', 55, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1434012422),
(102, 8, 1, 2130706433, 'attribute', 56, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1434012444),
(103, 8, 1, 2130706433, 'attribute', 57, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1434012463),
(104, 8, 1, 2130706433, 'attribute', 58, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1434012488),
(105, 8, 1, 2130706433, 'attribute', 59, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1434012503),
(106, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-11 16:53登錄了後臺', 1, 1434012783),
(107, 7, 1, 2130706433, 'model', 12, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434012828),
(108, 7, 1, 2130706433, 'model', 12, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434012863),
(109, 11, 1, 2130706433, 'category', 51, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1434012886),
(110, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-11 17:19登錄了後臺', 1, 1434014351),
(111, 8, 1, 2130706433, 'attribute', 60, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1434015047),
(112, 8, 1, 2130706433, 'attribute', 60, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1434015087),
(113, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 10:13登錄了後臺', 1, 1434075203),
(114, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 10:14登錄了後臺', 1, 1434075268),
(115, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 10:23登錄了後臺', 1, 1434075830),
(116, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 10:37登錄了後臺', 1, 1434076624),
(117, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 10:44登錄了後臺', 1, 1434077094),
(118, 10, 1, 2130706433, 'Menu', 2, '操作url：/cuiyufan/Admin/Menu/edit.html', 1, 1434077213),
(119, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 11:42登錄了後臺', 1, 1434080532),
(120, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 11:49登錄了後臺', 1, 1434080992),
(121, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 11:56登錄了後臺', 1, 1434081381),
(122, 7, 1, 2130706433, 'model', 1, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434089703),
(123, 7, 1, 2130706433, 'model', 1, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434089817),
(124, 7, 1, 2130706433, 'model', 1, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434092552),
(125, 7, 1, 2130706433, 'model', 1, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434092590),
(126, 10, 1, 2130706433, 'Menu', 2, '操作url：/cuiyufan/Admin/Menu/edit.html', 1, 1434092620),
(127, 8, 1, 2130706433, 'attribute', 61, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1434096622),
(128, 8, 1, 2130706433, 'attribute', 36, '操作url：/cuiyufan/Admin/Attribute/remove/id/36.html', 1, 1434097123),
(129, 7, 1, 2130706433, 'model', 6, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434097136),
(130, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 16:46登錄了後臺', 1, 1434098760),
(131, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-12 16:46登錄了後臺', 1, 1434098793),
(132, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-16 09:55登錄了後臺', 1, 1434419722),
(133, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-16 13:46登錄了後臺', 1, 1434433618),
(134, 6, 1, 2130706433, 'config', 43, '操作url：/cuiyufan/Admin/Config/edit.html', 1, 1434434225),
(135, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-16 16:41登錄了後臺', 1, 1434444116),
(136, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-16 17:45登錄了後臺', 1, 1434447902),
(137, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-17 10:52登錄了後臺', 1, 1434509529),
(138, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-17 14:09登錄了後臺', 1, 1434521346),
(139, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-18 09:59登錄了後臺', 1, 1434592770),
(140, 7, 1, 2130706433, 'model', 9, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434592843),
(141, 7, 1, 2130706433, 'model', 9, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434592858),
(142, 7, 1, 2130706433, 'model', 10, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434592872),
(143, 7, 1, 2130706433, 'model', 9, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434593039),
(144, 1, 1, 2130706433, 'member', 1, 'admin在2015-06-18 10:24登錄了後臺', 1, 1434594277),
(145, 7, 1, 2130706433, 'model', 12, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434594289),
(146, 7, 1, 2130706433, 'model', 11, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434594298),
(147, 7, 1, 2130706433, 'model', 7, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434594324),
(148, 7, 1, 2130706433, 'model', 5, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434594351),
(149, 7, 1, 2130706433, 'model', 4, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434594368),
(150, 7, 1, 2130706433, 'model', 3, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434594377),
(151, 7, 1, 2130706433, 'model', 2, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1434594387),
(152, 1, 1, 0, 'member', 1, 'admin在2015-07-02 10:46登錄了後臺', 1, 1435805160),
(153, 8, 1, 0, 'attribute', 33, '操作url：/cuiyufan/Admin/Attribute/remove/id/33.html', 1, 1435805445),
(154, 1, 1, 0, 'member', 1, 'admin在2015-07-02 10:51登錄了後臺', 1, 1435805509),
(155, 11, 1, 0, 'category', 2, '操作url：/cuiyufan/Admin/Category/remove/id/2.html', 1, 1435805570),
(156, 7, 1, 0, 'model', 13, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1435805598),
(157, 8, 1, 0, 'attribute', 62, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1435805697),
(158, 11, 1, 0, 'category', 52, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1435805717),
(159, 11, 1, 0, 'category', 52, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1435805728),
(160, 7, 1, 0, 'model', 13, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1435805751),
(161, 7, 1, 0, 'model', 13, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1435805775),
(162, 11, 1, 0, 'category', 52, '操作url：/cuiyufan/Admin/Category/remove/id/52.html', 1, 1435806080),
(163, 8, 1, 0, 'attribute', 62, '操作url：/cuiyufan/Admin/Attribute/remove/id/62.html', 1, 1435806086),
(164, 8, 1, 0, 'attribute', 63, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1435806105),
(165, 11, 1, 0, 'category', 53, '操作url：/cuiyufan/Admin/Category/add.html', 1, 1435806120),
(166, 8, 1, 0, 'attribute', 64, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1435807823),
(167, 8, 1, 0, 'attribute', 64, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1435808775),
(168, 8, 1, 0, 'attribute', 65, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1435808860),
(169, 8, 1, 0, 'attribute', 66, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1435808874),
(170, 7, 1, 0, 'model', 12, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1435808888),
(171, 7, 1, 2130706433, 'model', 2, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1436250821),
(172, 11, 1, 2130706433, 'category', 39, '操作url：/cuiyufan/Admin/Category/remove/id/39.html', 1, 1436251685),
(173, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-10 10:29登錄了後臺', 1, 1436495376),
(174, 8, 1, 2130706433, 'attribute', 34, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1436498189),
(175, 8, 1, 2130706433, 'attribute', 34, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1436498526),
(176, 8, 1, 2130706433, 'attribute', 67, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1436498805),
(177, 8, 1, 2130706433, 'attribute', 68, '操作url：/cuiyufan/Admin/Attribute/update.html', 1, 1436498818),
(178, 7, 1, 2130706433, 'model', 2, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1436498837),
(179, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-10 11:38登錄了後臺', 1, 1436499516),
(180, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-10 11:39登錄了後臺', 1, 1436499565),
(181, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-10 12:16登錄了後臺', 1, 1436501796),
(182, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-10 14:07登錄了後臺', 1, 1436508467),
(183, 7, 1, 2130706433, 'model', 6, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1436509524),
(184, 7, 1, 2130706433, 'model', 6, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1436509570),
(185, 11, 1, 2130706433, 'category', 45, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1436510247),
(186, 7, 1, 2130706433, 'model', 6, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1436511288),
(187, 7, 1, 2130706433, 'model', 6, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1436511324),
(188, 7, 1, 2130706433, 'model', 6, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1436511332),
(189, 11, 1, 2130706433, 'category', 45, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1436511384),
(190, 11, 1, 2130706433, 'category', 45, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1436511393),
(191, 11, 1, 2130706433, 'category', 45, '操作url：/cuiyufan/Admin/Category/edit.html', 1, 1436511403),
(192, 7, 1, 2130706433, 'model', 6, '操作url：/cuiyufan/Admin/Model/update.html', 1, 1436511721);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_addons`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(40) NOT NULL COMMENT '插件名或標識',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '狀態',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本號',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安裝時間',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有後臺列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='插件表' AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `cuiyufang_addons`
--

INSERT INTO `cuiyufang_addons` (`id`, `name`, `title`, `description`, `status`, `config`, `author`, `version`, `create_time`, `has_adminlist`) VALUES
(2, 'SiteStat', '站點統計信息', '統計站點的基礎信息', 1, '{"title":"\\u7cfb\\u7edf\\u4fe1\\u606f","width":"1","display":"1","status":"0"}', 'thinkphp', '0.1', 1379512015, 0),
(3, 'DevTeam', '開發團隊信息', '開發團隊成員信息', 1, '{"title":"OneThink\\u5f00\\u53d1\\u56e2\\u961f","width":"2","display":"1"}', 'thinkphp', '0.1', 1379512022, 0),
(4, 'SystemInfo', '系統環境信息', '用於顯示壹些服務器的信息', 1, '{"title":"\\u7cfb\\u7edf\\u4fe1\\u606f","width":"2","display":"1"}', 'thinkphp', '0.1', 1379512036, 0),
(5, 'Editor', '前臺編輯器', '用於增強整站長文本的輸入和顯示', 1, '{"editor_type":"2","editor_wysiwyg":"1","editor_height":"300px","editor_resize_type":"1"}', 'thinkphp', '0.1', 1379830910, 0),
(6, 'Attachment', '附件', '用於文檔模型上傳附件', 1, 'null', 'thinkphp', '0.1', 1379842319, 1),
(9, 'SocialComment', '通用社交化評論', '集成了各種社交化評論插件，輕松集成到系統中。', 1, '{"comment_type":"1","comment_uid_youyan":"","comment_short_name_duoshuo":"","comment_data_list_duoshuo":""}', 'thinkphp', '0.1', 1380273962, 0),
(15, 'EditorForAdmin', '後臺編輯器', '用於增強整站長文本的輸入和顯示', 1, '{"editor_type":"2","editor_wysiwyg":"1","editor_height":"500px","editor_resize_type":"1"}', 'thinkphp', '0.1', 1383126253, 0),
(17, 'UploadImages', '多圖上傳', '多圖上傳', 1, 'null', '木梁大囧', '1.2', 1435805389, 0);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_attachment`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用戶ID',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '附件顯示名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件類型',
  `source` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '資源ID',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '關聯記錄ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下載次數',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '附件大小',
  `dir` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '上級目錄ID',
  `sort` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '狀態',
  PRIMARY KEY (`id`),
  KEY `idx_record_status` (`record_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='附件表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_attribute`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '字段註釋',
  `field` varchar(100) NOT NULL DEFAULT '' COMMENT '字段定義',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '數據類型',
  `value` varchar(100) NOT NULL DEFAULT '' COMMENT '字段默認值',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '備註',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否顯示',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '參數',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `is_must` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `validate_rule` varchar(255) NOT NULL,
  `validate_time` tinyint(1) unsigned NOT NULL,
  `error_info` varchar(100) NOT NULL,
  `validate_type` varchar(25) NOT NULL,
  `auto_rule` varchar(100) NOT NULL,
  `auto_time` tinyint(1) unsigned NOT NULL,
  `auto_type` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='模型屬性表' AUTO_INCREMENT=69 ;

--
-- 转存表中的数据 `cuiyufang_attribute`
--

INSERT INTO `cuiyufang_attribute` (`id`, `name`, `title`, `field`, `type`, `value`, `remark`, `is_show`, `extra`, `model_id`, `is_must`, `status`, `update_time`, `create_time`, `validate_rule`, `validate_time`, `error_info`, `validate_type`, `auto_rule`, `auto_time`, `auto_type`) VALUES
(1, 'uid', '用戶ID', 'int(10) unsigned NOT NULL ', 'num', '0', '', 0, '', 1, 0, 1, 1384508362, 1383891233, '', 0, '', '', '', 0, ''),
(2, 'name', '標識', 'char(40) NOT NULL ', 'string', '', '同壹根節點下標識不重復', 1, '', 1, 0, 1, 1383894743, 1383891233, '', 0, '', '', '', 0, ''),
(3, 'title', '標題', 'char(80) NOT NULL ', 'string', '', '文檔標題', 1, '', 1, 0, 1, 1383894778, 1383891233, '', 0, '', '', '', 0, ''),
(4, 'category_id', '所屬分類', 'int(10) unsigned NOT NULL ', 'string', '', '', 0, '', 1, 0, 1, 1384508336, 1383891233, '', 0, '', '', '', 0, ''),
(5, 'description', '描述', 'char(140) NOT NULL ', 'textarea', '', '', 1, '', 1, 0, 1, 1383894927, 1383891233, '', 0, '', '', '', 0, ''),
(6, 'root', '根節點', 'int(10) unsigned NOT NULL ', 'num', '0', '該文檔的頂級文檔編號', 0, '', 1, 0, 1, 1384508323, 1383891233, '', 0, '', '', '', 0, ''),
(7, 'pid', '所屬ID', 'int(10) unsigned NOT NULL ', 'num', '0', '父文檔編號', 0, '', 1, 0, 1, 1384508543, 1383891233, '', 0, '', '', '', 0, ''),
(8, 'model_id', '內容模型ID', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '該文檔所對應的模型', 0, '', 1, 0, 1, 1384508350, 1383891233, '', 0, '', '', '', 0, ''),
(9, 'type', '內容類型', 'tinyint(3) unsigned NOT NULL ', 'select', '2', '', 1, '1:目錄\r\n2:主題\r\n3:段落', 1, 0, 1, 1384511157, 1383891233, '', 0, '', '', '', 0, ''),
(10, 'position', '推薦位', 'smallint(5) unsigned NOT NULL ', 'checkbox', '0', '多個推薦則將其推薦值相加', 1, '1:列表推薦\r\n2:頻道頁推薦\r\n4:首頁推薦', 1, 0, 1, 1383895640, 1383891233, '', 0, '', '', '', 0, ''),
(11, 'link_id', '外鏈', 'int(10) unsigned NOT NULL ', 'num', '0', '0-非外鏈，大於0-外鏈ID,需要函數進行鏈接與編號的轉換', 1, '', 1, 0, 1, 1383895757, 1383891233, '', 0, '', '', '', 0, ''),
(12, 'cover_id', '封面', 'int(10) unsigned NOT NULL ', 'picture', '0', '0-無封面，大於0-封面圖片ID，需要函數處理', 1, '', 1, 0, 1, 1384147827, 1383891233, '', 0, '', '', '', 0, ''),
(13, 'display', '可見性', 'tinyint(3) unsigned NOT NULL ', 'radio', '1', '', 1, '0:不可見\r\n1:所有人可見', 1, 0, 1, 1386662271, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
(14, 'deadline', '截至時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '0-永久有效', 1, '', 1, 0, 1, 1387163248, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
(15, 'attach', '附件數量', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '', 0, '', 1, 0, 1, 1387260355, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
(16, 'view', '瀏覽量', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 1, 0, 1, 1383895835, 1383891233, '', 0, '', '', '', 0, ''),
(17, 'comment', '評論數', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 1, 0, 1, 1383895846, 1383891233, '', 0, '', '', '', 0, ''),
(18, 'extend', '擴展統計字段', 'int(10) unsigned NOT NULL ', 'num', '0', '根據需求自行使用', 0, '', 1, 0, 1, 1384508264, 1383891233, '', 0, '', '', '', 0, ''),
(19, 'level', '優先級', 'int(10) unsigned NOT NULL ', 'num', '0', '越高排序越靠前', 1, '', 1, 0, 1, 1383895894, 1383891233, '', 0, '', '', '', 0, ''),
(20, 'create_time', '創建時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', 1, '', 1, 0, 1, 1383895903, 1383891233, '', 0, '', '', '', 0, ''),
(21, 'update_time', '更新時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', 0, '', 1, 0, 1, 1384508277, 1383891233, '', 0, '', '', '', 0, ''),
(22, 'status', '數據狀態', 'tinyint(4) NOT NULL ', 'radio', '0', '', 0, '-1:刪除\r\n0:禁用\r\n1:正常\r\n2:待審核\r\n3:草稿', 1, 0, 1, 1384508496, 1383891233, '', 0, '', '', '', 0, ''),
(23, 'parse', '內容解析類型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', 0, '0:html\r\n1:ubb\r\n2:markdown', 2, 0, 1, 1384511049, 1383891243, '', 0, '', '', '', 0, ''),
(24, 'content', '文章內容', 'text NOT NULL ', 'editor', '', '', 1, '', 2, 0, 1, 1383896225, 1383891243, '', 0, '', '', '', 0, ''),
(25, 'template', '詳情頁顯示模板', 'varchar(100) NOT NULL ', 'string', '', '參照display方法參數的定義', 1, '', 2, 0, 1, 1383896190, 1383891243, '', 0, '', '', '', 0, ''),
(26, 'bookmark', '收藏數', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 2, 0, 1, 1383896103, 1383891243, '', 0, '', '', '', 0, ''),
(27, 'parse', '內容解析類型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', 0, '0:html\r\n1:ubb\r\n2:markdown', 3, 0, 1, 1387260461, 1383891252, '', 0, '', 'regex', '', 0, 'function'),
(28, 'content', '下載詳細描述', 'text NOT NULL ', 'editor', '', '', 1, '', 3, 0, 1, 1383896438, 1383891252, '', 0, '', '', '', 0, ''),
(29, 'template', '詳情頁顯示模板', 'varchar(100) NOT NULL ', 'string', '', '', 1, '', 3, 0, 1, 1383896429, 1383891252, '', 0, '', '', '', 0, ''),
(30, 'file_id', '文件ID', 'int(10) unsigned NOT NULL ', 'file', '0', '需要函數處理', 1, '', 3, 0, 1, 1383896415, 1383891252, '', 0, '', '', '', 0, ''),
(31, 'download', '下載次數', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 3, 0, 1, 1383896380, 1383891252, '', 0, '', '', '', 0, ''),
(32, 'size', '文件大小', 'bigint(20) unsigned NOT NULL ', 'num', '0', '單位bit', 1, '', 3, 0, 1, 1383896371, 1383891252, '', 0, '', '', '', 0, ''),
(34, 'image', '圖片', 'varchar(255) NOT NULL', 'pictures', '', '', 1, '', 5, 0, 1, 1436498526, 1433476460, '', 3, '', 'regex', '', 3, 'function'),
(35, 'content', '介紹', 'text NOT NULL', 'editor', '', '', 1, '', 5, 0, 1, 1433476485, 1433476485, '', 3, '', 'regex', '', 3, 'function'),
(37, 'attribute', '屬性', 'tinyint(2) NOT NULL', 'bool', '', '', 1, '', 6, 0, 1, 1433476628, 1433476628, '', 3, '', 'regex', '', 3, 'function'),
(38, 'recom', '是否推薦', 'char(10) NOT NULL', 'radio', '', '', 1, '0:是\r\n1:否', 6, 0, 1, 1433476680, 1433476680, '', 3, '', 'regex', '', 3, 'function'),
(39, 'image', '圖片', 'int(10) UNSIGNED NOT NULL', 'picture', '', '', 1, '', 6, 0, 1, 1433476713, 1433476713, '', 3, '', 'regex', '', 3, 'function'),
(40, 'content', '詳細描述', 'text NOT NULL', 'editor', '', '', 1, '', 6, 0, 1, 1433476732, 1433476732, '', 3, '', 'regex', '', 3, 'function'),
(41, 'phone', '電話', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 7, 0, 1, 1433476841, 1433476841, '', 3, '', 'regex', '', 3, 'function'),
(42, 'Email', 'E-mail', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 7, 0, 1, 1433476858, 1433476858, '', 3, '', 'regex', '', 3, 'function'),
(43, 'fax', '傳真', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 7, 0, 1, 1433476876, 1433476876, '', 3, '', 'regex', '', 3, 'function'),
(44, 'address', '地址', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 7, 0, 1, 1433476890, 1433476890, '', 3, '', 'regex', '', 3, 'function'),
(45, 'location', '地理位置', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 7, 0, 1, 1433476922, 1433476922, '', 3, '', 'regex', '', 3, 'function'),
(46, 'name', '姓名', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 8, 0, 1, 1433477357, 1433477357, '', 3, '', 'regex', '', 3, 'function'),
(47, 'phone', '電話', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 8, 0, 1, 1433477370, 1433477370, '', 3, '', 'regex', '', 3, 'function'),
(48, 'email', '郵箱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 8, 0, 1, 1433477389, 1433477389, '', 3, '', 'regex', '', 3, 'function'),
(49, 'content', '內容', 'text NOT NULL', 'textarea', '', '', 1, '', 8, 0, 1, 1433477412, 1433477412, '', 3, '', 'regex', '', 3, 'function'),
(50, 'time', '時間', 'int(10) NOT NULL', 'datetime', '', '', 1, '', 8, 0, 1, 1433477449, 1433477449, '', 3, '', 'regex', '', 3, 'function'),
(51, 'status', '狀態', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 8, 0, 1, 1433477470, 1433477470, '', 3, '', 'regex', '', 3, 'function'),
(52, 'tname', '類別名稱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 9, 0, 1, 1433477510, 1433477510, '', 3, '', 'regex', '', 3, 'function'),
(53, 'aname', '屬性名稱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 10, 0, 1, 1433477603, 1433477603, '', 3, '', 'regex', '', 3, 'function'),
(54, 'logo', '圖片', 'int(10) UNSIGNED NOT NULL', 'picture', '', '', 1, '', 11, 0, 1, 1433477688, 1433477688, '', 3, '', 'regex', '', 3, 'function'),
(55, 'WEB_SITE_TITLE', '網站標題', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 12, 0, 1, 1434012422, 1434012422, '', 3, '', 'regex', '', 3, 'function'),
(56, 'ANALYTICS', 'google analytics配置', 'text NOT NULL', 'textarea', '', '', 1, '', 12, 0, 1, 1434012445, 1434012445, '', 3, '', 'regex', '', 3, 'function'),
(57, 'DESCRIPTION', '網站描述', 'text NOT NULL', 'textarea', '', '', 1, '', 12, 0, 1, 1434012463, 1434012463, '', 3, '', 'regex', '', 3, 'function'),
(58, 'KEYWORD', '網站關鍵字', 'text NOT NULL', 'textarea', '', '', 1, '', 12, 0, 1, 1434012488, 1434012488, '', 3, '', 'regex', '', 3, 'function'),
(59, 'copyright', '版權信息', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 12, 0, 1, 1434012503, 1434012503, '', 3, '', 'regex', '', 3, 'function'),
(60, 'view', '瀏覽量', 'int(10) unsigned NOT NULL ', 'num', '', '', 1, '', 8, 0, 1, 1434015087, 1434015047, '', 3, '', 'regex', '', 3, 'function'),
(61, 'types', '種類', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 1, 0, 1, 1434096622, 1434096622, '', 3, '', 'regex', '', 3, 'function'),
(63, 'pics', '上傳圖片', 'varchar(100) NOT NULL', 'pictures', '', '', 1, '', 13, 0, 1, 1435806105, 1435806105, '', 3, '', 'regex', '', 3, 'function'),
(64, 'foot', '底部信息', 'text NOT NULL', 'editor', '', '', 1, '', 12, 0, 1, 1435808775, 1435807823, '', 3, '', 'regex', '', 3, 'function'),
(65, 'Facebook', 'Facebook鏈接', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 12, 0, 1, 1435808860, 1435808860, '', 3, '', 'regex', '', 3, 'function'),
(66, 'Youtube', 'Youtube鏈接', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 12, 0, 1, 1435808874, 1435808874, '', 3, '', 'regex', '', 3, 'function'),
(67, 'come', '來源', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 2, 0, 1, 1436498805, 1436498805, '', 3, '', 'regex', '', 3, 'function'),
(68, 'editer', '作者', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 2, 0, 1, 1436498818, 1436498818, '', 3, '', 'regex', '', 3, 'function');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_auth_extend`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_auth_extend` (
  `group_id` mediumint(10) unsigned NOT NULL COMMENT '用戶id',
  `extend_id` mediumint(8) unsigned NOT NULL COMMENT '擴展表中數據的id',
  `type` tinyint(1) unsigned NOT NULL COMMENT '擴展類型標識 1:欄目分類權限;2:模型權限',
  UNIQUE KEY `group_extend_type` (`group_id`,`extend_id`,`type`),
  KEY `uid` (`group_id`),
  KEY `group_id` (`extend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用戶組與分類的對應關系表';

--
-- 转存表中的数据 `cuiyufang_auth_extend`
--

INSERT INTO `cuiyufang_auth_extend` (`group_id`, `extend_id`, `type`) VALUES
(1, 1, 1),
(1, 1, 2),
(1, 2, 1),
(1, 2, 2),
(1, 3, 1),
(1, 3, 2),
(1, 4, 1),
(1, 37, 1),
(2, 1, 1),
(2, 2, 1),
(2, 39, 1),
(2, 40, 1),
(2, 41, 1),
(2, 42, 1),
(2, 43, 1),
(2, 44, 1),
(2, 45, 1),
(2, 46, 1),
(2, 47, 1),
(2, 48, 1),
(2, 49, 1),
(2, 50, 1),
(2, 51, 1);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_auth_group`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶組id,自增主鍵',
  `module` varchar(20) NOT NULL COMMENT '用戶組所屬模塊',
  `type` tinyint(4) NOT NULL COMMENT '組類型',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '用戶組中文名稱',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用戶組狀態：為1正常，為0禁用,-1為刪除',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用戶組擁有的規則id，多個規則 , 隔開',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `cuiyufang_auth_group`
--

INSERT INTO `cuiyufang_auth_group` (`id`, `module`, `type`, `title`, `description`, `status`, `rules`) VALUES
(1, 'admin', 1, '默認用戶組', '', 1, '1,2,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,96,97,100,102,103,105,106'),
(2, 'admin', 1, '測試用戶', '測試用戶', 1, '1,7,8,9,10,11,12,13,14,15,16,17,18,79,211,217,218,219');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_auth_group_access`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '用戶id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用戶組id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `cuiyufang_auth_group_access`
--

INSERT INTO `cuiyufang_auth_group_access` (`uid`, `group_id`) VALUES
(2, 2);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_auth_rule`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '規則id,自增主鍵',
  `module` varchar(20) NOT NULL COMMENT '規則所屬module',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;2-主菜單',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '規則唯壹英文標識',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '規則中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:無效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '規則附加條件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=220 ;

--
-- 转存表中的数据 `cuiyufang_auth_rule`
--

INSERT INTO `cuiyufang_auth_rule` (`id`, `module`, `type`, `name`, `title`, `status`, `condition`) VALUES
(1, 'admin', 2, 'Admin/Index/index', '首頁', 1, ''),
(2, 'admin', 2, 'Admin/Article/mydocument', '內容', -1, ''),
(3, 'admin', 2, 'Admin/User/index', '用戶', 1, ''),
(4, 'admin', 2, 'Admin/Addons/index', '擴展', 1, ''),
(5, 'admin', 2, 'Admin/Config/group', '系統', 1, ''),
(7, 'admin', 1, 'Admin/article/add', '新增', 1, ''),
(8, 'admin', 1, 'Admin/article/edit', '編輯', 1, ''),
(9, 'admin', 1, 'Admin/article/setStatus', '改變狀態', 1, ''),
(10, 'admin', 1, 'Admin/article/update', '保存', 1, ''),
(11, 'admin', 1, 'Admin/article/autoSave', '保存草稿', 1, ''),
(12, 'admin', 1, 'Admin/article/move', '移動', 1, ''),
(13, 'admin', 1, 'Admin/article/copy', '復制', 1, ''),
(14, 'admin', 1, 'Admin/article/paste', '粘貼', 1, ''),
(15, 'admin', 1, 'Admin/article/permit', '還原', 1, ''),
(16, 'admin', 1, 'Admin/article/clear', '清空', 1, ''),
(17, 'admin', 1, 'Admin/article/index', '文檔列表', 1, ''),
(18, 'admin', 1, 'Admin/article/recycle', '回收站', 1, ''),
(19, 'admin', 1, 'Admin/User/addaction', '新增用戶行為', 1, ''),
(20, 'admin', 1, 'Admin/User/editaction', '編輯用戶行為', 1, ''),
(21, 'admin', 1, 'Admin/User/saveAction', '保存用戶行為', 1, ''),
(22, 'admin', 1, 'Admin/User/setStatus', '變更行為狀態', 1, ''),
(23, 'admin', 1, 'Admin/User/changeStatus?method=forbidUser', '禁用會員', 1, ''),
(24, 'admin', 1, 'Admin/User/changeStatus?method=resumeUser', '啟用會員', 1, ''),
(25, 'admin', 1, 'Admin/User/changeStatus?method=deleteUser', '刪除會員', 1, ''),
(26, 'admin', 1, 'Admin/User/index', '用戶信息', 1, ''),
(27, 'admin', 1, 'Admin/User/action', '用戶行為', 1, ''),
(28, 'admin', 1, 'Admin/AuthManager/changeStatus?method=deleteGroup', '刪除', 1, ''),
(29, 'admin', 1, 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', 1, ''),
(30, 'admin', 1, 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢復', 1, ''),
(31, 'admin', 1, 'Admin/AuthManager/createGroup', '新增', 1, ''),
(32, 'admin', 1, 'Admin/AuthManager/editGroup', '編輯', 1, ''),
(33, 'admin', 1, 'Admin/AuthManager/writeGroup', '保存用戶組', 1, ''),
(34, 'admin', 1, 'Admin/AuthManager/group', '授權', 1, ''),
(35, 'admin', 1, 'Admin/AuthManager/access', '訪問授權', 1, ''),
(36, 'admin', 1, 'Admin/AuthManager/user', '成員授權', 1, ''),
(37, 'admin', 1, 'Admin/AuthManager/removeFromGroup', '解除授權', 1, ''),
(38, 'admin', 1, 'Admin/AuthManager/addToGroup', '保存成員授權', 1, ''),
(39, 'admin', 1, 'Admin/AuthManager/category', '分類授權', 1, ''),
(40, 'admin', 1, 'Admin/AuthManager/addToCategory', '保存分類授權', 1, ''),
(41, 'admin', 1, 'Admin/AuthManager/index', '權限管理', 1, ''),
(42, 'admin', 1, 'Admin/Addons/create', '創建', 1, ''),
(43, 'admin', 1, 'Admin/Addons/checkForm', '檢測創建', 1, ''),
(44, 'admin', 1, 'Admin/Addons/preview', '預覽', 1, ''),
(45, 'admin', 1, 'Admin/Addons/build', '快速生成插件', 1, ''),
(46, 'admin', 1, 'Admin/Addons/config', '設置', 1, ''),
(47, 'admin', 1, 'Admin/Addons/disable', '禁用', 1, ''),
(48, 'admin', 1, 'Admin/Addons/enable', '啟用', 1, ''),
(49, 'admin', 1, 'Admin/Addons/install', '安裝', 1, ''),
(50, 'admin', 1, 'Admin/Addons/uninstall', '卸載', 1, ''),
(51, 'admin', 1, 'Admin/Addons/saveconfig', '更新配置', 1, ''),
(52, 'admin', 1, 'Admin/Addons/adminList', '插件後臺列表', 1, ''),
(53, 'admin', 1, 'Admin/Addons/execute', 'URL方式訪問插件', 1, ''),
(54, 'admin', 1, 'Admin/Addons/index', '插件管理', 1, ''),
(55, 'admin', 1, 'Admin/Addons/hooks', '鉤子管理', 1, ''),
(56, 'admin', 1, 'Admin/model/add', '新增', 1, ''),
(57, 'admin', 1, 'Admin/model/edit', '編輯', 1, ''),
(58, 'admin', 1, 'Admin/model/setStatus', '改變狀態', 1, ''),
(59, 'admin', 1, 'Admin/model/update', '保存數據', 1, ''),
(60, 'admin', 1, 'Admin/Model/index', '模型管理', 1, ''),
(61, 'admin', 1, 'Admin/Config/edit', '編輯', 1, ''),
(62, 'admin', 1, 'Admin/Config/del', '刪除', 1, ''),
(63, 'admin', 1, 'Admin/Config/add', '新增', 1, ''),
(64, 'admin', 1, 'Admin/Config/save', '保存', 1, ''),
(65, 'admin', 1, 'Admin/Config/group', '網站設置', 1, ''),
(66, 'admin', 1, 'Admin/Config/index', '配置管理', 1, ''),
(67, 'admin', 1, 'Admin/Channel/add', '新增', 1, ''),
(68, 'admin', 1, 'Admin/Channel/edit', '編輯', 1, ''),
(69, 'admin', 1, 'Admin/Channel/del', '刪除', 1, ''),
(70, 'admin', 1, 'Admin/Channel/index', '導航管理', 1, ''),
(71, 'admin', 1, 'Admin/Category/edit', '編輯', 1, ''),
(72, 'admin', 1, 'Admin/Category/add', '新增', 1, ''),
(73, 'admin', 1, 'Admin/Category/remove', '刪除', 1, ''),
(74, 'admin', 1, 'Admin/Category/index', '分類管理', 1, ''),
(75, 'admin', 1, 'Admin/file/upload', '上傳控件', -1, ''),
(76, 'admin', 1, 'Admin/file/uploadPicture', '上傳圖片', -1, ''),
(77, 'admin', 1, 'Admin/file/download', '下載', -1, ''),
(79, 'admin', 1, 'Admin/article/batchOperate', '導入', 1, ''),
(80, 'admin', 1, 'Admin/Database/index?type=export', '備份數據庫', 1, ''),
(81, 'admin', 1, 'Admin/Database/index?type=import', '還原數據庫', 1, ''),
(82, 'admin', 1, 'Admin/Database/export', '備份', 1, ''),
(83, 'admin', 1, 'Admin/Database/optimize', '優化表', 1, ''),
(84, 'admin', 1, 'Admin/Database/repair', '修復表', 1, ''),
(86, 'admin', 1, 'Admin/Database/import', '恢復', 1, ''),
(87, 'admin', 1, 'Admin/Database/del', '刪除', 1, ''),
(88, 'admin', 1, 'Admin/User/add', '新增用戶', 1, ''),
(89, 'admin', 1, 'Admin/Attribute/index', '屬性管理', 1, ''),
(90, 'admin', 1, 'Admin/Attribute/add', '新增', 1, ''),
(91, 'admin', 1, 'Admin/Attribute/edit', '編輯', 1, ''),
(92, 'admin', 1, 'Admin/Attribute/setStatus', '改變狀態', 1, ''),
(93, 'admin', 1, 'Admin/Attribute/update', '保存數據', 1, ''),
(94, 'admin', 1, 'Admin/AuthManager/modelauth', '模型授權', 1, ''),
(95, 'admin', 1, 'Admin/AuthManager/addToModel', '保存模型授權', 1, ''),
(96, 'admin', 1, 'Admin/Category/move', '移動', -1, ''),
(97, 'admin', 1, 'Admin/Category/merge', '合並', -1, ''),
(98, 'admin', 1, 'Admin/Config/menu', '後臺菜單管理', -1, ''),
(99, 'admin', 1, 'Admin/Article/mydocument', '內容', -1, ''),
(100, 'admin', 1, 'Admin/Menu/index', '菜單管理', 1, ''),
(101, 'admin', 1, 'Admin/other', '其他', -1, ''),
(102, 'admin', 1, 'Admin/Menu/add', '新增', 1, ''),
(103, 'admin', 1, 'Admin/Menu/edit', '編輯', 1, ''),
(104, 'admin', 1, 'Admin/Think/lists?model=article', '文章管理', -1, ''),
(105, 'admin', 1, 'Admin/Think/lists?model=download', '下載管理', 1, ''),
(106, 'admin', 1, 'Admin/Think/lists?model=config', '配置管理', 1, ''),
(107, 'admin', 1, 'Admin/Action/actionlog', '行為日誌', 1, ''),
(108, 'admin', 1, 'Admin/User/updatePassword', '修改密碼', 1, ''),
(109, 'admin', 1, 'Admin/User/updateNickname', '修改昵稱', 1, ''),
(110, 'admin', 1, 'Admin/action/edit', '查看行為日誌', 1, ''),
(111, 'admin', 2, 'Admin/article/index', '文檔列表', -1, ''),
(112, 'admin', 2, 'Admin/article/add', '新增', -1, ''),
(113, 'admin', 2, 'Admin/article/edit', '編輯', -1, ''),
(114, 'admin', 2, 'Admin/article/setStatus', '改變狀態', -1, ''),
(115, 'admin', 2, 'Admin/article/update', '保存', -1, ''),
(116, 'admin', 2, 'Admin/article/autoSave', '保存草稿', -1, ''),
(117, 'admin', 2, 'Admin/article/move', '移動', -1, ''),
(118, 'admin', 2, 'Admin/article/copy', '復制', -1, ''),
(119, 'admin', 2, 'Admin/article/paste', '粘貼', -1, ''),
(120, 'admin', 2, 'Admin/article/batchOperate', '導入', -1, ''),
(121, 'admin', 2, 'Admin/article/recycle', '回收站', -1, ''),
(122, 'admin', 2, 'Admin/article/permit', '還原', -1, ''),
(123, 'admin', 2, 'Admin/article/clear', '清空', -1, ''),
(124, 'admin', 2, 'Admin/User/add', '新增用戶', -1, ''),
(125, 'admin', 2, 'Admin/User/action', '用戶行為', -1, ''),
(126, 'admin', 2, 'Admin/User/addAction', '新增用戶行為', -1, ''),
(127, 'admin', 2, 'Admin/User/editAction', '編輯用戶行為', -1, ''),
(128, 'admin', 2, 'Admin/User/saveAction', '保存用戶行為', -1, ''),
(129, 'admin', 2, 'Admin/User/setStatus', '變更行為狀態', -1, ''),
(130, 'admin', 2, 'Admin/User/changeStatus?method=forbidUser', '禁用會員', -1, ''),
(131, 'admin', 2, 'Admin/User/changeStatus?method=resumeUser', '啟用會員', -1, ''),
(132, 'admin', 2, 'Admin/User/changeStatus?method=deleteUser', '刪除會員', -1, ''),
(133, 'admin', 2, 'Admin/AuthManager/index', '權限管理', -1, ''),
(134, 'admin', 2, 'Admin/AuthManager/changeStatus?method=deleteGroup', '刪除', -1, ''),
(135, 'admin', 2, 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', -1, ''),
(136, 'admin', 2, 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢復', -1, ''),
(137, 'admin', 2, 'Admin/AuthManager/createGroup', '新增', -1, ''),
(138, 'admin', 2, 'Admin/AuthManager/editGroup', '編輯', -1, ''),
(139, 'admin', 2, 'Admin/AuthManager/writeGroup', '保存用戶組', -1, ''),
(140, 'admin', 2, 'Admin/AuthManager/group', '授權', -1, ''),
(141, 'admin', 2, 'Admin/AuthManager/access', '訪問授權', -1, ''),
(142, 'admin', 2, 'Admin/AuthManager/user', '成員授權', -1, ''),
(143, 'admin', 2, 'Admin/AuthManager/removeFromGroup', '解除授權', -1, ''),
(144, 'admin', 2, 'Admin/AuthManager/addToGroup', '保存成員授權', -1, ''),
(145, 'admin', 2, 'Admin/AuthManager/category', '分類授權', -1, ''),
(146, 'admin', 2, 'Admin/AuthManager/addToCategory', '保存分類授權', -1, ''),
(147, 'admin', 2, 'Admin/AuthManager/modelauth', '模型授權', -1, ''),
(148, 'admin', 2, 'Admin/AuthManager/addToModel', '保存模型授權', -1, ''),
(149, 'admin', 2, 'Admin/Addons/create', '創建', -1, ''),
(150, 'admin', 2, 'Admin/Addons/checkForm', '檢測創建', -1, ''),
(151, 'admin', 2, 'Admin/Addons/preview', '預覽', -1, ''),
(152, 'admin', 2, 'Admin/Addons/build', '快速生成插件', -1, ''),
(153, 'admin', 2, 'Admin/Addons/config', '設置', -1, ''),
(154, 'admin', 2, 'Admin/Addons/disable', '禁用', -1, ''),
(155, 'admin', 2, 'Admin/Addons/enable', '啟用', -1, ''),
(156, 'admin', 2, 'Admin/Addons/install', '安裝', -1, ''),
(157, 'admin', 2, 'Admin/Addons/uninstall', '卸載', -1, ''),
(158, 'admin', 2, 'Admin/Addons/saveconfig', '更新配置', -1, ''),
(159, 'admin', 2, 'Admin/Addons/adminList', '插件後臺列表', -1, ''),
(160, 'admin', 2, 'Admin/Addons/execute', 'URL方式訪問插件', -1, ''),
(161, 'admin', 2, 'Admin/Addons/hooks', '鉤子管理', -1, ''),
(162, 'admin', 2, 'Admin/Model/index', '模型管理', -1, ''),
(163, 'admin', 2, 'Admin/model/add', '新增', -1, ''),
(164, 'admin', 2, 'Admin/model/edit', '編輯', -1, ''),
(165, 'admin', 2, 'Admin/model/setStatus', '改變狀態', -1, ''),
(166, 'admin', 2, 'Admin/model/update', '保存數據', -1, ''),
(167, 'admin', 2, 'Admin/Attribute/index', '屬性管理', -1, ''),
(168, 'admin', 2, 'Admin/Attribute/add', '新增', -1, ''),
(169, 'admin', 2, 'Admin/Attribute/edit', '編輯', -1, ''),
(170, 'admin', 2, 'Admin/Attribute/setStatus', '改變狀態', -1, ''),
(171, 'admin', 2, 'Admin/Attribute/update', '保存數據', -1, ''),
(172, 'admin', 2, 'Admin/Config/index', '配置管理', -1, ''),
(173, 'admin', 2, 'Admin/Config/edit', '編輯', -1, ''),
(174, 'admin', 2, 'Admin/Config/del', '刪除', -1, ''),
(175, 'admin', 2, 'Admin/Config/add', '新增', -1, ''),
(176, 'admin', 2, 'Admin/Config/save', '保存', -1, ''),
(177, 'admin', 2, 'Admin/Menu/index', '菜單管理', -1, ''),
(178, 'admin', 2, 'Admin/Channel/index', '導航管理', -1, ''),
(179, 'admin', 2, 'Admin/Channel/add', '新增', -1, ''),
(180, 'admin', 2, 'Admin/Channel/edit', '編輯', -1, ''),
(181, 'admin', 2, 'Admin/Channel/del', '刪除', -1, ''),
(182, 'admin', 2, 'Admin/Category/index', '分類管理', -1, ''),
(183, 'admin', 2, 'Admin/Category/edit', '編輯', -1, ''),
(184, 'admin', 2, 'Admin/Category/add', '新增', -1, ''),
(185, 'admin', 2, 'Admin/Category/remove', '刪除', -1, ''),
(186, 'admin', 2, 'Admin/Category/move', '移動', -1, ''),
(187, 'admin', 2, 'Admin/Category/merge', '合並', -1, ''),
(188, 'admin', 2, 'Admin/Database/index?type=export', '備份數據庫', -1, ''),
(189, 'admin', 2, 'Admin/Database/export', '備份', -1, ''),
(190, 'admin', 2, 'Admin/Database/optimize', '優化表', -1, ''),
(191, 'admin', 2, 'Admin/Database/repair', '修復表', -1, ''),
(192, 'admin', 2, 'Admin/Database/index?type=import', '還原數據庫', -1, ''),
(193, 'admin', 2, 'Admin/Database/import', '恢復', -1, ''),
(194, 'admin', 2, 'Admin/Database/del', '刪除', -1, ''),
(195, 'admin', 2, 'Admin/other', '其他', 1, ''),
(196, 'admin', 2, 'Admin/Menu/add', '新增', -1, ''),
(197, 'admin', 2, 'Admin/Menu/edit', '編輯', -1, ''),
(198, 'admin', 2, 'Admin/Think/lists?model=article', '應用', -1, ''),
(199, 'admin', 2, 'Admin/Think/lists?model=download', '下載管理', -1, ''),
(200, 'admin', 2, 'Admin/Think/lists?model=config', '應用', -1, ''),
(201, 'admin', 2, 'Admin/Action/actionlog', '行為日誌', -1, ''),
(202, 'admin', 2, 'Admin/User/updatePassword', '修改密碼', -1, ''),
(203, 'admin', 2, 'Admin/User/updateNickname', '修改昵稱', -1, ''),
(204, 'admin', 2, 'Admin/action/edit', '查看行為日誌', -1, ''),
(205, 'admin', 1, 'Admin/think/add', '新增數據', 1, ''),
(206, 'admin', 1, 'Admin/think/edit', '編輯數據', 1, ''),
(207, 'admin', 1, 'Admin/Menu/import', '導入', 1, ''),
(208, 'admin', 1, 'Admin/Model/generate', '生成', 1, ''),
(209, 'admin', 1, 'Admin/Addons/addHook', '新增鉤子', 1, ''),
(210, 'admin', 1, 'Admin/Addons/edithook', '編輯鉤子', 1, ''),
(211, 'admin', 1, 'Admin/Article/sort', '文檔排序', 1, ''),
(212, 'admin', 1, 'Admin/Config/sort', '排序', 1, ''),
(213, 'admin', 1, 'Admin/Menu/sort', '排序', 1, ''),
(214, 'admin', 1, 'Admin/Channel/sort', '排序', 1, ''),
(215, 'admin', 1, 'Admin/Category/operate/type/move', '移動', 1, ''),
(216, 'admin', 1, 'Admin/Category/operate/type/merge', '合並', 1, ''),
(217, 'admin', 1, 'Admin/Article/feedback', '留言板', 1, ''),
(218, 'admin', 1, 'Admin/Article/feedbackdetail', '查看留言', 1, ''),
(219, 'admin', 2, 'Admin/Article/feedback', '內容', 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_category`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分類ID',
  `name` varchar(30) NOT NULL COMMENT '標誌',
  `title` varchar(50) NOT NULL COMMENT '標題',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級分類ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同級有效）',
  `list_row` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表每頁行數',
  `meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的網頁標題',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '關鍵字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `template_index` varchar(100) NOT NULL COMMENT '頻道頁模板',
  `template_lists` varchar(100) NOT NULL COMMENT '列表頁模板',
  `template_detail` varchar(100) NOT NULL COMMENT '詳情頁模板',
  `template_edit` varchar(100) NOT NULL COMMENT '編輯頁模板',
  `model` varchar(100) NOT NULL DEFAULT '' COMMENT '關聯模型',
  `type` varchar(100) NOT NULL DEFAULT '' COMMENT '允許發布的內容類型',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外鏈',
  `allow_publish` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允許發布內容',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '可見性',
  `reply` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允許回復',
  `check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '發布的文章是否需要審核',
  `reply_model` varchar(100) NOT NULL DEFAULT '',
  `extend` text NOT NULL COMMENT '擴展設置',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '數據狀態',
  `icon` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分類圖標',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='分類表' AUTO_INCREMENT=54 ;

--
-- 转存表中的数据 `cuiyufang_category`
--

INSERT INTO `cuiyufang_category` (`id`, `name`, `title`, `pid`, `sort`, `list_row`, `meta_title`, `keywords`, `description`, `template_index`, `template_lists`, `template_detail`, `template_edit`, `model`, `type`, `link_id`, `allow_publish`, `display`, `reply`, `check`, `reply_model`, `extend`, `create_time`, `update_time`, `status`, `icon`) VALUES
(1, 'blog', '首頁', 0, 0, 10, '', '', '', '', '', '', '', '2', '2,1', 0, 0, 1, 0, 0, '1', '', 1379474947, 1433478393, 1, 0),
(40, 'about', '關於我們', 0, 1, 10, '', '', '', '', '', '', '', '5', '', 0, 1, 1, 1, 0, '', '', 1433478643, 1433478648, 1, 0),
(41, 'news', '最新動態', 0, 2, 10, '', '', '', '', '', '', '', '2', '', 0, 1, 1, 1, 0, '', '', 1433478685, 1433478690, 1, 0),
(42, 'product', '翡翠產品', 0, 3, 10, '', '', '', '', '', '', '', '', '2', 0, 0, 1, 1, 0, '', '', 1433478768, 1433478787, 1, 0),
(43, 'type', '產品分類', 42, 0, 10, '', '', '', '', '', '', '', '9', '2', 0, 1, 1, 1, 0, '', '', 1433478819, 1433483477, 1, 0),
(44, 'attribute', '產品屬性', 42, 1, 10, '', '', '', '', '', '', '', '10', '2', 0, 1, 1, 1, 0, '', '', 1433478847, 1433485182, 1, 0),
(45, 'products', '產品列表', 42, 2, 10, '', '', '', '', '', '', '', '6', '2', 0, 1, 1, 1, 0, '', '', 1433478873, 1436511403, 1, 0),
(46, 'contact', '聯繫我們', 0, 4, 10, '', '', '', '', '', '', '', '', '2', 0, 0, 1, 1, 0, '', '', 1433478914, 1433478926, 1, 0),
(47, 'information', '聯繫資料', 46, 1, 10, '', '', '', '', '', '', '', '7', '2', 0, 1, 1, 1, 0, '', '', 1433478979, 1433483077, 1, 0),
(48, 'conditions', '條款細則', 46, 0, 10, '', '', '', '', '', '', '', '2', '2', 0, 1, 1, 1, 0, '', '', 1433479028, 1433479028, 1, 0),
(49, 'config', '網站設置', 0, 5, 10, '', '', '', '', '', '', '', '', '2', 0, 0, 1, 1, 0, '', '', 1434012030, 1434012041, 1, 0),
(50, 'logo', 'logo設置', 49, 0, 10, '', '', '', '', '', '', '', '11', '2', 0, 1, 1, 1, 0, '', '', 1434012107, 1434012107, 1, 0),
(51, 'conf', '基本資料', 49, 0, 10, '', '', '', '', '', '', '', '12', '2', 0, 1, 1, 1, 0, '', '', 1434012194, 1434012886, 1, 0),
(53, 'ad', '廣告', 1, 0, 10, '', '', '', '', '', '', '', '13', '', 0, 1, 1, 1, 0, '', '', 1435806120, 1435806120, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_channel`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '頻道ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級頻道ID',
  `title` char(30) NOT NULL COMMENT '頻道標題',
  `url` char(100) NOT NULL COMMENT '頻道連接',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '導航排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '狀態',
  `target` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '新窗口打開',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `cuiyufang_channel`
--

INSERT INTO `cuiyufang_channel` (`id`, `pid`, `title`, `url`, `sort`, `create_time`, `update_time`, `status`, `target`) VALUES
(1, 0, '首頁', 'Index/index', 1, 1379475111, 1379923177, 1, 0),
(2, 0, '關於我們', 'Index/about', 2, 1379475131, 1433484035, 1, 0),
(3, 0, '最新動態', 'Index/news', 3, 1379475154, 1433484091, 1, 0),
(4, 0, '翡翠產品', 'Index/products', 4, 1433484117, 1433484117, 1, 0),
(5, 0, '聯繫我們', 'Index/contact', 5, 1433484143, 1433484143, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_config`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名稱',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置類型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置說明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分組',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL COMMENT '配置說明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '狀態',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- 转存表中的数据 `cuiyufang_config`
--

INSERT INTO `cuiyufang_config` (`id`, `name`, `type`, `title`, `group`, `extra`, `remark`, `create_time`, `update_time`, `status`, `value`, `sort`) VALUES
(1, 'WEB_SITE_TITLE', 1, '網站標題', 1, '', '網站標題前臺顯示標題', 1378898976, 1379235274, 1, '翠玉坊', 0),
(2, 'WEB_SITE_DESCRIPTION', 2, '網站描述', 1, '', '網站搜索引擎描述', 1378898976, 1379235841, 1, '翠玉坊', 1),
(3, 'WEB_SITE_KEYWORD', 2, '網站關鍵字', 1, '', '網站搜索引擎關鍵字', 1378898976, 1381390100, 1, '翠玉坊', 8),
(4, 'WEB_SITE_CLOSE', 4, '關閉站點', 1, '0:關閉,1:開啟', '站點關閉後其他用戶不能訪問，管理員可以正常訪問', 1378898976, 1379235296, 1, '1', 1),
(9, 'CONFIG_TYPE_LIST', 3, '配置類型列表', 4, '', '主要用於數據解析和頁面表單的生成', 1378898976, 1433555169, 1, '0:數字\r\n1:字符\r\n2:文本\r\n3:數組\r\n4:枚舉\r\n5:非明文', 2),
(10, 'WEB_SITE_ICP', 1, '網站備案號', 1, '', '設置在網站底部顯示的備案號，如“滬ICP備12007941號-2', 1378900335, 1379235859, 1, '', 9),
(11, 'DOCUMENT_POSITION', 3, '文檔推薦位', 2, '', '文檔推薦位，推薦到多個位置KEY值相加即可', 1379053380, 1379235329, 1, '1:列表頁推薦\r\n2:頻道頁推薦\r\n4:網站首頁推薦', 3),
(12, 'DOCUMENT_DISPLAY', 3, '文檔可見性', 2, '', '文章可見性僅影響前臺顯示，後臺不收影響', 1379056370, 1379235322, 1, '0:所有人可見\r\n1:僅註冊會員可見\r\n2:僅管理員可見', 4),
(13, 'COLOR_STYLE', 4, '後臺色系', 1, 'default_color:默認\r\nblue_color:紫羅蘭', '後臺顏色風格', 1379122533, 1379235904, 1, 'default_color', 10),
(20, 'CONFIG_GROUP_LIST', 3, '配置分組', 4, '', '配置分組', 1379228036, 1433485817, 1, '1:基本\r\n2:內容\r\n3:用戶\r\n4:系統\r\n5:郵件', 4),
(21, 'HOOKS_TYPE', 3, '鉤子的類型', 4, '', '類型 1-用於擴展顯示內容，2-用於擴展業務處理', 1379313397, 1379313407, 1, '1:視圖\r\n2:控制器', 6),
(22, 'AUTH_CONFIG', 3, 'Auth配置', 4, '', '自定義Auth.class.php類配置', 1379409310, 1379409564, 1, 'AUTH_ON:1\r\nAUTH_TYPE:2', 8),
(23, 'OPEN_DRAFTBOX', 4, '是否開啟草稿功能', 2, '0:關閉草稿功能\r\n1:開啟草稿功能\r\n', '新增文章時的草稿功能配置', 1379484332, 1379484591, 1, '1', 1),
(24, 'DRAFT_AOTOSAVE_INTERVAL', 0, '自動保存草稿時間', 2, '', '自動保存草稿的時間間隔，單位：秒', 1379484574, 1386143323, 1, '60', 2),
(25, 'LIST_ROWS', 0, '後臺每頁記錄數', 2, '', '後臺數據每頁顯示記錄數', 1379503896, 1380427745, 1, '10', 10),
(26, 'USER_ALLOW_REGISTER', 4, '是否允許用戶註冊', 3, '0:關閉註冊\r\n1:允許註冊', '是否開放用戶註冊', 1379504487, 1379504580, 1, '1', 3),
(27, 'CODEMIRROR_THEME', 4, '預覽插件的CodeMirror主題', 4, '3024-day:3024 day\r\n3024-night:3024 night\r\nambiance:ambiance\r\nbase16-dark:base16 dark\r\nbase16-light:base16 light\r\nblackboard:blackboard\r\ncobalt:cobalt\r\neclipse:eclipse\r\nelegant:elegant\r\nerlang-dark:erlang-dark\r\nlesser-dark:lesser-dark\r\nmidnight:midnight', '詳情見CodeMirror官網', 1379814385, 1384740813, 1, 'ambiance', 3),
(28, 'DATA_BACKUP_PATH', 1, '數據庫備份根路徑', 4, '', '路徑必須以 / 結尾', 1381482411, 1381482411, 1, './Data/', 5),
(29, 'DATA_BACKUP_PART_SIZE', 0, '數據庫備份卷大小', 4, '', '該值用於限制壓縮後的分卷最大長度。單位：B；建議設置20M', 1381482488, 1381729564, 1, '20971520', 7),
(30, 'DATA_BACKUP_COMPRESS', 4, '數據庫備份文件是否啟用壓縮', 4, '0:不壓縮\r\n1:啟用壓縮', '壓縮備份文件需要PHP環境支持gzopen,gzwrite函數', 1381713345, 1381729544, 1, '1', 9),
(31, 'DATA_BACKUP_COMPRESS_LEVEL', 4, '數據庫備份文件壓縮級別', 4, '1:普通\r\n4:壹般\r\n9:最高', '數據庫備份文件的壓縮級別，該配置在開啟壓縮時生效', 1381713408, 1381713408, 1, '9', 10),
(32, 'DEVELOP_MODE', 4, '開啟開發者模式', 4, '0:關閉\r\n1:開啟', '是否開啟開發者模式', 1383105995, 1383291877, 1, '1', 11),
(33, 'ALLOW_VISIT', 3, '不受限控制器方法', 0, '', '', 1386644047, 1386644741, 1, '0:article/draftbox\r\n1:article/mydocument\r\n2:Category/tree\r\n3:Index/verify\r\n4:file/upload\r\n5:file/download\r\n6:user/updatePassword\r\n7:user/updateNickname\r\n8:user/submitPassword\r\n9:user/submitNickname\r\n10:file/uploadpicture', 0),
(34, 'DENY_VISIT', 3, '超管專限控制器方法', 0, '', '僅超級管理員可訪問的控制器方法', 1386644141, 1386644659, 1, '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook\r\n4:Admin/getMenus\r\n5:Admin/recordList\r\n6:AuthManager/updateRules\r\n7:AuthManager/tree', 0),
(35, 'REPLY_LIST_ROWS', 0, '回復列表每頁條數', 2, '', '', 1386645376, 1387178083, 1, '10', 0),
(36, 'ADMIN_ALLOW_IP', 2, '後臺允許訪問IP', 4, '', '多個用逗號分隔，如果不配置表示不限制IP訪問', 1387165454, 1387165553, 1, '', 12),
(37, 'SHOW_PAGE_TRACE', 4, '是否顯示頁面Trace', 4, '0:關閉\r\n1:開啟', '是否顯示頁面Trace信息', 1387165685, 1387165685, 1, '0', 1),
(38, 'MAIL_TYPE', 4, '郵件類型', 5, 'SMTP模塊發送', '如果您采用了服務器內置的Mail服務，不用配置該項', 1433485880, 1433485880, 1, '', 0),
(39, 'MAIL_SMTP_HOST', 1, 'SMTP服務器', 5, '', 'SMTP服務器', 1433485913, 1433485913, 1, 'smtp.163.com', 0),
(40, 'MAIL_SMTP_PORT', 0, 'SMTP服務器端口', 5, '', '默認25', 1433485962, 1433485962, 1, '25', 0),
(41, 'MAIL_SMTP_USER', 1, 'SMTP服務器用護名', 5, '', '填寫完整用護名', 1433486001, 1433486001, 1, 'moon1061031727@163.com', 0),
(42, 'MAIL_SMTP_PASS', 5, 'SMTP服務器密碼', 5, '', '填寫您的密碼', 1433486034, 1433555673, 1, 'sxvwavccnpvluuye', 0),
(43, 'MAIL_SMTP_RECEIVE', 1, '郵件接收地址', 5, '', '填寫接收郵件地址', 1433486064, 1434434225, 1, 'moon1061031727@163.com', 0),
(44, 'MAIL_SMTP_SUBJECT', 1, '郵件主題', 5, '', '填寫發送郵件的標題', 1433486101, 1433486101, 1, '有人反饋', 0),
(45, 'MAIL_SMTP_BODY', 2, '郵件內容', 5, '', '發送郵件的內容', 1433486132, 1433555726, 1, '有人反饋了壹條信息', 0);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文檔ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用戶ID',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '標識',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '標題',
  `category_id` int(10) unsigned NOT NULL COMMENT '所屬分類',
  `description` char(140) NOT NULL DEFAULT '' COMMENT '描述',
  `root` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '根節點',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所屬ID',
  `model_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容模型ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '內容類型',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '推薦位',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外鏈',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '可見性',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '截至時間',
  `attach` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件數量',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '瀏覽量',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '評論數',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '擴展統計字段',
  `level` int(10) NOT NULL DEFAULT '0' COMMENT '優先級',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '數據狀態',
  `types` varchar(255) NOT NULL COMMENT '種類',
  PRIMARY KEY (`id`),
  KEY `idx_category_status` (`category_id`,`status`),
  KEY `idx_status_type_pid` (`status`,`uid`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='文檔模型基礎表' AUTO_INCREMENT=37 ;

--
-- 转存表中的数据 `cuiyufang_document`
--

INSERT INTO `cuiyufang_document` (`id`, `uid`, `name`, `title`, `category_id`, `description`, `root`, `pid`, `model_id`, `type`, `position`, `link_id`, `cover_id`, `display`, `deadline`, `attach`, `view`, `comment`, `extend`, `level`, `create_time`, `update_time`, `status`, `types`) VALUES
(3, 1, '', '聯繫資料', 47, '', 0, 0, 7, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1433483340, 1436253000, 1, ''),
(4, 1, '', '條款', 48, '', 0, 0, 2, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1433483427, 1433483427, 1, ''),
(5, 1, '', '關於我們', 40, '', 0, 0, 5, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1433484420, 1436498535, 1, ''),
(10, 1, '', '新闻1', 41, '新闻1新闻1新闻1新闻1新闻1新闻1', 0, 0, 2, 2, 0, 0, 6, 1, 0, 0, 0, 0, 0, 0, 1433484840, 1436498866, 1, ''),
(11, 1, '', '擺件', 43, '', 0, 0, 9, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 5, 1433485108, 1433485108, 1, ''),
(12, 1, '', '玉鐲', 43, '', 0, 0, 9, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1433485121, 1433485121, 1, ''),
(13, 1, '', '吊墜', 43, '', 0, 0, 9, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 1433485135, 1433485135, 1, ''),
(14, 1, '', '項鏈', 43, '', 0, 0, 9, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 3, 1433485153, 1433485153, 1, ''),
(15, 1, '', '玻璃種', 44, '', 0, 0, 10, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 1433485208, 1433485208, 1, ''),
(16, 1, '', '豆種', 44, '', 0, 0, 10, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1433485220, 1433485220, 1, ''),
(17, 1, '', '糯種', 44, '', 0, 0, 10, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 3, 1433485231, 1433485231, 1, ''),
(18, 1, '', '冰種', 44, '', 0, 0, 10, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 4, 1433485247, 1433485247, 1, ''),
(19, 1, '', '瑪瑙手鐲', 45, '漂亮', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1433485380, 1434087501, 1, '12'),
(20, 1, '', '翡翠玉鐲', 45, '漂亮，高貴', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1433570580, 1434520174, 1, '12'),
(21, 1, '', 'logo', 50, '', 0, 0, 11, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1434012147, 1434012147, 1, ''),
(22, 1, '', '網站設置', 51, '', 0, 0, 12, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1434012900, 1435809015, 1, ''),
(23, 1, '', '瑪瑙項鏈', 45, '漂亮', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1434096660, 1434098379, 1, '14'),
(24, 1, '', '瑪瑙吊墜', 45, '可愛', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1434097320, 1434098331, 1, '13'),
(28, 1, '', '廣告', 53, '', 0, 0, 13, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1435806120, 1435806580, 1, ''),
(29, 1, '', '新闻2', 41, '新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新2', 0, 0, 2, 2, 0, 0, 5, 1, 0, 0, 0, 0, 0, 0, 1436250840, 1436498859, 1, ''),
(30, 1, '', '新闻2', 41, '新闻2新闻2新闻2新闻2新闻2新闻2新闻2', 0, 0, 2, 2, 0, 0, 5, 1, 0, 0, 0, 0, 0, 0, 1436250840, 1436498848, 1, ''),
(31, 1, '', '獨有款色', 43, '', 0, 0, 9, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 4, 1436250946, 1436250946, 1, ''),
(35, 1, '', '福運來珠寶11', 45, '福運來珠寶', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1436496540, 1436497557, 1, '31'),
(33, 1, '', '福運來珠寶', 45, '福運來珠寶福運來珠寶福運來珠寶', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1436250960, 1436495593, 1, '31'),
(34, 1, '', '福運來珠寶', 45, '福運來珠寶福運來珠寶福運來珠寶', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1436250960, 1436495587, 1, '31');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_ad`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `pics` varchar(100) NOT NULL COMMENT '上傳圖片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=29 ;

--
-- 转存表中的数据 `cuiyufang_document_ad`
--

INSERT INTO `cuiyufang_document_ad` (`id`, `pics`) VALUES
(28, '2,3');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_article`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_article` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文檔ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容解析類型',
  `content` text NOT NULL COMMENT '文章內容',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '詳情頁顯示模板',
  `bookmark` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏數',
  `come` varchar(255) NOT NULL COMMENT '來源',
  `editer` varchar(255) NOT NULL COMMENT '作者',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文檔模型文章表';

--
-- 转存表中的数据 `cuiyufang_document_article`
--

INSERT INTO `cuiyufang_document_article` (`id`, `parse`, `content`, `template`, `bookmark`, `come`, `editer`) VALUES
(4, 0, '<p>\r\n	1.以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠\r\n</p>\r\n<p>\r\n	2.以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠\r\n</p>\r\n<p>\r\n	3.以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠\r\n</p>\r\n<p>\r\n	4.以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠\r\n</p>\r\n<p>\r\n	5.以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠\r\n</p>', '', 0, '', ''),
(10, 0, '以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠寶,向溫婉嫵媚的女性特質致敬.壹款玲瓏剔透,光芒四射的珠寶, \r\n凝聚精湛工藝,璀璨奪目,極具象征意義以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠寶,向溫婉嫵媚的女性特質致敬.壹款玲瓏剔透,光芒四射的珠\r\n寶, 凝聚精湛工藝,璀璨奪目,極具象征意義意義意義', '', 0, '', '新闻1'),
(29, 0, '新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2', '', 0, '隨便', '隨便'),
(30, 0, '新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2新闻2', '', 0, '隨便', '隨便');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_attribute`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `aname` varchar(255) NOT NULL COMMENT '屬性名稱',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `cuiyufang_document_attribute`
--

INSERT INTO `cuiyufang_document_attribute` (`id`, `aname`) VALUES
(15, '玻璃種'),
(16, '豆種'),
(17, '糯種'),
(18, '冰種');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_config`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `WEB_SITE_TITLE` varchar(255) NOT NULL COMMENT '網站標題',
  `ANALYTICS` text NOT NULL COMMENT 'google analytics配置',
  `DESCRIPTION` text NOT NULL COMMENT '網站描述',
  `KEYWORD` text NOT NULL COMMENT '網站關鍵字',
  `copyright` varchar(255) NOT NULL COMMENT '版權信息',
  `foot` text NOT NULL COMMENT '底部信息',
  `Facebook` varchar(255) NOT NULL COMMENT 'Facebook鏈接',
  `Youtube` varchar(255) NOT NULL COMMENT 'Youtube鏈接',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=23 ;

--
-- 转存表中的数据 `cuiyufang_document_config`
--

INSERT INTO `cuiyufang_document_config` (`id`, `WEB_SITE_TITLE`, `ANALYTICS`, `DESCRIPTION`, `KEYWORD`, `copyright`, `foot`, `Facebook`, `Youtube`) VALUES
(22, '翠玉坊', '#', '翠玉坊', '翠玉坊', '© PIAGT 2015(1.12.9)', '<p>\r\n	以翠玉坊的特有方式詮釋珠寶工藝\r\n打造彌足珍貴的奢華珠寶,向溫婉嫵媚的女性特質致敬.\r\n</p>\r\n<p>\r\n	&nbsp; &nbsp; 一款玲瓏剔透,光芒四射的珠寶, 凝聚精湛工藝,璀璨奪目,極具象征意義\r\n</p>', 'http://www.facebook.com', 'http://www.youtube.com');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_contact`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `phone` varchar(255) NOT NULL COMMENT '電話',
  `Email` varchar(255) NOT NULL COMMENT 'E-mail',
  `fax` varchar(255) NOT NULL COMMENT '傳真',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `location` varchar(255) NOT NULL COMMENT '地理位置',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `cuiyufang_document_contact`
--

INSERT INTO `cuiyufang_document_contact` (`id`, `phone`, `Email`, `fax`, `address`, `location`) VALUES
(3, '123456788', '123456@gmail.com', '+81 123456', '澳門某某路某街58號商鋪', '22.4832969725,113.3907069871');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_download`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_download` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文檔ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容解析類型',
  `content` text NOT NULL COMMENT '下載詳細描述',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '詳情頁顯示模板',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下載次數',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文檔模型下載表';

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_image`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `image` varchar(255) NOT NULL COMMENT '圖片',
  `content` text NOT NULL COMMENT '介紹',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `cuiyufang_document_image`
--

INSERT INTO `cuiyufang_document_image` (`id`, `image`, `content`) VALUES
(5, '1,5,6', '以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠寶,向溫婉嫵媚的女性特質致敬.壹款玲瓏剔透,光芒四射的珠寶, \r\n凝聚精湛工藝,璀璨奪目,極具象征意義以翠玉坊的特有方式詮釋珠寶工藝打造彌足珍貴的奢華珠寶,向溫婉嫵媚的女性特質致敬.壹款玲瓏剔透,光芒四射的珠\r\n寶, 凝聚精湛工藝,璀璨奪目,極具象征意義');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_logo`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_logo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `logo` int(10) unsigned NOT NULL COMMENT '圖片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=22 ;

--
-- 转存表中的数据 `cuiyufang_document_logo`
--

INSERT INTO `cuiyufang_document_logo` (`id`, `logo`) VALUES
(21, 8);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_product`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `attribute` tinyint(2) NOT NULL COMMENT '屬性',
  `recom` char(10) NOT NULL COMMENT '是否推薦',
  `image` int(10) unsigned NOT NULL COMMENT '圖片',
  `content` text NOT NULL COMMENT '詳細描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=37 ;

--
-- 转存表中的数据 `cuiyufang_document_product`
--

INSERT INTO `cuiyufang_document_product` (`id`, `attribute`, `recom`, `image`, `content`) VALUES
(19, 17, '0', 1, '漂亮漂亮漂亮'),
(20, 18, '0', 1, '漂亮，高貴，漂亮，高貴，漂亮，高貴，漂亮，高貴'),
(23, 15, '0', 1, '很漂亮'),
(24, 16, '0', 1, '可愛可愛'),
(33, 18, '0', 6, '<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>\r\n<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>\r\n<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>\r\n<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>'),
(34, 16, '0', 1, '<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>\r\n<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>\r\n<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>\r\n<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>\r\n<h3 style="text-align:left;font-family:Arial, SimHei;font-weight:500;font-size:15px;background-color:#FFFFFF;">\r\n	福運來珠寶\r\n</h3>'),
(35, 18, '0', 6, '<a href="/cuiyufan/Admin/Article/edit/id/34/model/1/cate_id/45.html">福運來珠寶</a><a href="/cuiyufan/Admin/Article/edit/id/34/model/1/cate_id/45.html">福運來珠寶</a><a href="/cuiyufan/Admin/Article/edit/id/34/model/1/cate_id/45.html">福運來珠寶</a><a href="/cuiyufan/Admin/Article/edit/id/34/model/1/cate_id/45.html">福運來珠寶</a>');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_document_type`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_document_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `tname` varchar(255) NOT NULL COMMENT '類別名稱',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=33 ;

--
-- 转存表中的数据 `cuiyufang_document_type`
--

INSERT INTO `cuiyufang_document_type` (`id`, `tname`) VALUES
(11, '擺件'),
(12, '玉鐲'),
(13, '吊墜'),
(14, '項鏈'),
(31, '獨有款色');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_feedback`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `phone` varchar(255) NOT NULL COMMENT '電話',
  `email` varchar(255) NOT NULL COMMENT '郵箱',
  `content` text NOT NULL COMMENT '內容',
  `time` int(10) NOT NULL COMMENT '時間',
  `status` varchar(255) NOT NULL COMMENT '狀態',
  `view` int(10) unsigned NOT NULL COMMENT '瀏覽量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `cuiyufang_feedback`
--

INSERT INTO `cuiyufang_feedback` (`id`, `name`, `phone`, `email`, `content`, `time`, `status`, `view`) VALUES
(1, '12', '12', '121', '12445', 1433486151, '1', 3),
(2, 'd', '545464677', 'fdsfasd@qq.com', '457487', 1434015287, '1', 1),
(3, '12', '12', '3214', '43214', 1434362079, '1', 0),
(4, 'uyre', 'u6rt', 'utr', 'irt', 1434362122, '1', 1),
(5, 'iuy', 'oiyt', 'oty', 'oyt', 1434362184, '1', 0),
(6, '345', '4353', '32', '32132', 1434362214, '1', 1),
(7, '7546', '645', '6456', '65465', 1434362257, '1', 0),
(8, '41521', '124', '1231', '01213', 1434362394, '1', 0),
(9, '543', '534', '543', '543543', 1434362411, '1', 2),
(10, 'sf', 'sd', 'ds', 'dsd', 1434419877, '1', 1),
(11, 'sf', 'sd', '10614145@qq.com', 'dsd', 1434419895, '1', 2),
(12, 'sfds', 'fsad', '1061414455@qq.com', 'dsd', 1434419916, '1', 74);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_file`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名稱',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路徑',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件後綴',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime類型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1編碼',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `create_time` int(10) unsigned NOT NULL COMMENT '上傳時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_md5` (`md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_hooks`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '鉤子名稱',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '類型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '鉤子掛載的插件 ''，''分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `cuiyufang_hooks`
--

INSERT INTO `cuiyufang_hooks` (`id`, `name`, `description`, `type`, `update_time`, `addons`) VALUES
(1, 'pageHeader', '頁面header鉤子，壹般用於加載插件CSS文件和代碼', 1, 0, ''),
(2, 'pageFooter', '頁面footer鉤子，壹般用於加載插件JS文件和JS代碼', 1, 0, 'ReturnTop'),
(3, 'documentEditForm', '添加編輯表單的 擴展內容鉤子', 1, 0, 'Attachment'),
(4, 'documentDetailAfter', '文檔末尾顯示', 1, 0, 'Attachment,SocialComment'),
(5, 'documentDetailBefore', '頁面內容前顯示用鉤子', 1, 0, ''),
(6, 'documentSaveComplete', '保存文檔數據後的擴展鉤子', 2, 0, 'Attachment'),
(7, 'documentEditFormContent', '添加編輯表單的內容顯示鉤子', 1, 0, 'Editor'),
(8, 'adminArticleEdit', '後臺內容編輯頁編輯器', 1, 1378982734, 'EditorForAdmin'),
(13, 'AdminIndex', '首頁小格子個性化顯示', 1, 1382596073, 'SiteStat,SystemInfo,DevTeam'),
(14, 'topicComment', '評論提交方式擴展鉤子。', 1, 1380163518, 'Editor'),
(16, 'app_begin', '應用開始', 2, 1384481614, ''),
(18, 'UploadImages', 'UploadImages', 1, 1435805380, 'UploadImages');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_member`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶ID',
  `nickname` char(16) NOT NULL DEFAULT '' COMMENT '昵稱',
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性別',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq號',
  `score` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用戶積分',
  `login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登錄次數',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '註冊IP',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '註冊時間',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最後登錄IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最後登錄時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '會員狀態',
  PRIMARY KEY (`uid`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='會員表' AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `cuiyufang_member`
--

INSERT INTO `cuiyufang_member` (`uid`, `nickname`, `sex`, `birthday`, `qq`, `score`, `login`, `reg_ip`, `reg_time`, `last_login_ip`, `last_login_time`, `status`) VALUES
(1, 'admin', 0, '0000-00-00', '', 60, 36, 0, 1433475305, 2130706433, 1436508467, 1),
(2, 'user', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_menu`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文檔ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '標題',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級分類ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同級有效）',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '鏈接地址',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隱藏',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '提示',
  `group` varchar(50) DEFAULT '' COMMENT '分組',
  `is_dev` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否僅開發者模式可見',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=124 ;

--
-- 转存表中的数据 `cuiyufang_menu`
--

INSERT INTO `cuiyufang_menu` (`id`, `title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`) VALUES
(1, '首頁', 0, 1, 'Index/index', 1, '', '', 0),
(2, '內容', 0, 2, 'Article/feedback', 0, '', '', 0),
(3, '文檔列表', 2, 0, 'article/index', 1, '', '內容', 0),
(4, '新增', 3, 0, 'article/add', 0, '', '', 0),
(5, '編輯', 3, 0, 'article/edit', 0, '', '', 0),
(6, '改變狀態', 3, 0, 'article/setStatus', 0, '', '', 0),
(7, '保存', 3, 0, 'article/update', 0, '', '', 0),
(8, '保存草稿', 3, 0, 'article/autoSave', 0, '', '', 0),
(9, '移動', 3, 0, 'article/move', 0, '', '', 0),
(10, '復制', 3, 0, 'article/copy', 0, '', '', 0),
(11, '粘貼', 3, 0, 'article/paste', 0, '', '', 0),
(12, '導入', 3, 0, 'article/batchOperate', 0, '', '', 0),
(13, '回收站', 2, 0, 'article/recycle', 1, '', '內容', 0),
(14, '還原', 13, 0, 'article/permit', 0, '', '', 0),
(15, '清空', 13, 0, 'article/clear', 0, '', '', 0),
(16, '用戶', 0, 3, 'User/index', 0, '', '', 0),
(17, '用戶信息', 16, 0, 'User/index', 0, '', '用戶管理', 0),
(18, '新增用戶', 17, 0, 'User/add', 0, '添加新用戶', '', 0),
(19, '用戶行為', 16, 0, 'User/action', 0, '', '行為管理', 0),
(20, '新增用戶行為', 19, 0, 'User/addaction', 0, '', '', 0),
(21, '編輯用戶行為', 19, 0, 'User/editaction', 0, '', '', 0),
(22, '保存用戶行為', 19, 0, 'User/saveAction', 0, '"用戶->用戶行為"保存編輯和新增的用戶行為', '', 0),
(23, '變更行為狀態', 19, 0, 'User/setStatus', 0, '"用戶->用戶行為"中的啟用,禁用和刪除權限', '', 0),
(24, '禁用會員', 19, 0, 'User/changeStatus?method=forbidUser', 0, '"用戶->用戶信息"中的禁用', '', 0),
(25, '啟用會員', 19, 0, 'User/changeStatus?method=resumeUser', 0, '"用戶->用戶信息"中的啟用', '', 0),
(26, '刪除會員', 19, 0, 'User/changeStatus?method=deleteUser', 0, '"用戶->用戶信息"中的刪除', '', 0),
(27, '權限管理', 16, 0, 'AuthManager/index', 0, '', '用戶管理', 0),
(28, '刪除', 27, 0, 'AuthManager/changeStatus?method=deleteGroup', 0, '刪除用戶組', '', 0),
(29, '禁用', 27, 0, 'AuthManager/changeStatus?method=forbidGroup', 0, '禁用用戶組', '', 0),
(30, '恢復', 27, 0, 'AuthManager/changeStatus?method=resumeGroup', 0, '恢復已禁用的用戶組', '', 0),
(31, '新增', 27, 0, 'AuthManager/createGroup', 0, '創建新的用戶組', '', 0),
(32, '編輯', 27, 0, 'AuthManager/editGroup', 0, '編輯用戶組名稱和描述', '', 0),
(33, '保存用戶組', 27, 0, 'AuthManager/writeGroup', 0, '新增和編輯用戶組的"保存"按鈕', '', 0),
(34, '授權', 27, 0, 'AuthManager/group', 0, '"後臺 \\ 用戶 \\ 用戶信息"列表頁的"授權"操作按鈕,用於設置用戶所屬用戶組', '', 0),
(35, '訪問授權', 27, 0, 'AuthManager/access', 0, '"後臺 \\ 用戶 \\ 權限管理"列表頁的"訪問授權"操作按鈕', '', 0),
(36, '成員授權', 27, 0, 'AuthManager/user', 0, '"後臺 \\ 用戶 \\ 權限管理"列表頁的"成員授權"操作按鈕', '', 0),
(37, '解除授權', 27, 0, 'AuthManager/removeFromGroup', 0, '"成員授權"列表頁內的解除授權操作按鈕', '', 0),
(38, '保存成員授權', 27, 0, 'AuthManager/addToGroup', 0, '"用戶信息"列表頁"授權"時的"保存"按鈕和"成員授權"裏右上角的"添加"按鈕)', '', 0),
(39, '分類授權', 27, 0, 'AuthManager/category', 0, '"後臺 \\ 用戶 \\ 權限管理"列表頁的"分類授權"操作按鈕', '', 0),
(40, '保存分類授權', 27, 0, 'AuthManager/addToCategory', 0, '"分類授權"頁面的"保存"按鈕', '', 0),
(41, '模型授權', 27, 0, 'AuthManager/modelauth', 0, '"後臺 \\ 用戶 \\ 權限管理"列表頁的"模型授權"操作按鈕', '', 0),
(42, '保存模型授權', 27, 0, 'AuthManager/addToModel', 0, '"分類授權"頁面的"保存"按鈕', '', 0),
(43, '擴展', 0, 7, 'Addons/index', 0, '', '', 0),
(44, '插件管理', 43, 1, 'Addons/index', 0, '', '擴展', 0),
(45, '創建', 44, 0, 'Addons/create', 0, '服務器上創建插件結構向導', '', 0),
(46, '檢測創建', 44, 0, 'Addons/checkForm', 0, '檢測插件是否可以創建', '', 0),
(47, '預覽', 44, 0, 'Addons/preview', 0, '預覽插件定義類文件', '', 0),
(48, '快速生成插件', 44, 0, 'Addons/build', 0, '開始生成插件結構', '', 0),
(49, '設置', 44, 0, 'Addons/config', 0, '設置插件配置', '', 0),
(50, '禁用', 44, 0, 'Addons/disable', 0, '禁用插件', '', 0),
(51, '啟用', 44, 0, 'Addons/enable', 0, '啟用插件', '', 0),
(52, '安裝', 44, 0, 'Addons/install', 0, '安裝插件', '', 0),
(53, '卸載', 44, 0, 'Addons/uninstall', 0, '卸載插件', '', 0),
(54, '更新配置', 44, 0, 'Addons/saveconfig', 0, '更新插件配置處理', '', 0),
(55, '插件後臺列表', 44, 0, 'Addons/adminList', 0, '', '', 0),
(56, 'URL方式訪問插件', 44, 0, 'Addons/execute', 0, '控制是否有權限通過url訪問插件控制器方法', '', 0),
(57, '鉤子管理', 43, 2, 'Addons/hooks', 0, '', '擴展', 0),
(58, '模型管理', 68, 3, 'Model/index', 0, '', '系統設置', 0),
(59, '新增', 58, 0, 'model/add', 0, '', '', 0),
(60, '編輯', 58, 0, 'model/edit', 0, '', '', 0),
(61, '改變狀態', 58, 0, 'model/setStatus', 0, '', '', 0),
(62, '保存數據', 58, 0, 'model/update', 0, '', '', 0),
(63, '屬性管理', 68, 0, 'Attribute/index', 1, '網站屬性配置。', '', 0),
(64, '新增', 63, 0, 'Attribute/add', 0, '', '', 0),
(65, '編輯', 63, 0, 'Attribute/edit', 0, '', '', 0),
(66, '改變狀態', 63, 0, 'Attribute/setStatus', 0, '', '', 0),
(67, '保存數據', 63, 0, 'Attribute/update', 0, '', '', 0),
(68, '系統', 0, 4, 'Config/group', 0, '', '', 0),
(69, '網站設置', 68, 1, 'Config/group', 0, '', '系統設置', 0),
(70, '配置管理', 68, 4, 'Config/index', 0, '', '系統設置', 0),
(71, '編輯', 70, 0, 'Config/edit', 0, '新增編輯和保存配置', '', 0),
(72, '刪除', 70, 0, 'Config/del', 0, '刪除配置', '', 0),
(73, '新增', 70, 0, 'Config/add', 0, '新增配置', '', 0),
(74, '保存', 70, 0, 'Config/save', 0, '保存配置', '', 0),
(75, '菜單管理', 68, 5, 'Menu/index', 0, '', '系統設置', 0),
(76, '導航管理', 68, 6, 'Channel/index', 0, '', '系統設置', 0),
(77, '新增', 76, 0, 'Channel/add', 0, '', '', 0),
(78, '編輯', 76, 0, 'Channel/edit', 0, '', '', 0),
(79, '刪除', 76, 0, 'Channel/del', 0, '', '', 0),
(80, '分類管理', 68, 2, 'Category/index', 0, '', '系統設置', 0),
(81, '編輯', 80, 0, 'Category/edit', 0, '編輯和保存欄目分類', '', 0),
(82, '新增', 80, 0, 'Category/add', 0, '新增欄目分類', '', 0),
(83, '刪除', 80, 0, 'Category/remove', 0, '刪除欄目分類', '', 0),
(84, '移動', 80, 0, 'Category/operate/type/move', 0, '移動欄目分類', '', 0),
(85, '合並', 80, 0, 'Category/operate/type/merge', 0, '合並欄目分類', '', 0),
(86, '備份數據庫', 68, 0, 'Database/index?type=export', 0, '', '數據備份', 0),
(87, '備份', 86, 0, 'Database/export', 0, '備份數據庫', '', 0),
(88, '優化表', 86, 0, 'Database/optimize', 0, '優化數據表', '', 0),
(89, '修復表', 86, 0, 'Database/repair', 0, '修復數據表', '', 0),
(90, '還原數據庫', 68, 0, 'Database/index?type=import', 0, '', '數據備份', 0),
(91, '恢復', 90, 0, 'Database/import', 0, '數據庫恢復', '', 0),
(92, '刪除', 90, 0, 'Database/del', 0, '刪除備份文件', '', 0),
(93, '其他', 0, 5, 'other', 1, '', '', 0),
(96, '新增', 75, 0, 'Menu/add', 0, '', '系統設置', 0),
(98, '編輯', 75, 0, 'Menu/edit', 0, '', '', 0),
(104, '下載管理', 102, 0, 'Think/lists?model=download', 0, '', '', 0),
(105, '配置管理', 102, 0, 'Think/lists?model=config', 0, '', '', 0),
(106, '行為日誌', 16, 0, 'Action/actionlog', 0, '', '行為管理', 0),
(108, '修改密碼', 16, 0, 'User/updatePassword', 1, '', '', 0),
(109, '修改昵稱', 16, 0, 'User/updateNickname', 1, '', '', 0),
(110, '查看行為日誌', 106, 0, 'action/edit', 1, '', '', 0),
(112, '新增數據', 58, 0, 'think/add', 1, '', '', 0),
(113, '編輯數據', 58, 0, 'think/edit', 1, '', '', 0),
(114, '導入', 75, 0, 'Menu/import', 0, '', '', 0),
(115, '生成', 58, 0, 'Model/generate', 0, '', '', 0),
(116, '新增鉤子', 57, 0, 'Addons/addHook', 0, '', '', 0),
(117, '編輯鉤子', 57, 0, 'Addons/edithook', 0, '', '', 0),
(118, '文檔排序', 3, 0, 'Article/sort', 1, '', '', 0),
(119, '排序', 70, 0, 'Config/sort', 1, '', '', 0),
(120, '排序', 75, 0, 'Menu/sort', 1, '', '', 0),
(121, '排序', 76, 0, 'Channel/sort', 1, '', '', 0),
(122, '留言板', 2, 0, 'Article/feedback', 1, '留言板', '內容', 0),
(123, '查看留言', 122, 0, 'Article/feedbackdetail', 1, '留言板詳情', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_model`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '模型標識',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '模型名稱',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '繼承的模型',
  `relation` varchar(30) NOT NULL DEFAULT '' COMMENT '繼承與被繼承模型的關聯字段',
  `need_pk` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新建表時是否需要主鍵字段',
  `field_sort` text NOT NULL COMMENT '表單字段排序',
  `field_group` varchar(255) NOT NULL DEFAULT '1:基礎' COMMENT '字段分組',
  `attribute_list` text NOT NULL COMMENT '屬性列表（表的字段）',
  `template_list` varchar(100) NOT NULL DEFAULT '' COMMENT '列表模板',
  `template_add` varchar(100) NOT NULL DEFAULT '' COMMENT '新增模板',
  `template_edit` varchar(100) NOT NULL DEFAULT '' COMMENT '編輯模板',
  `list_grid` text NOT NULL COMMENT '列表定義',
  `list_row` smallint(2) unsigned NOT NULL DEFAULT '10' COMMENT '列表數據長度',
  `search_key` varchar(50) NOT NULL DEFAULT '' COMMENT '默認搜索字段',
  `search_list` varchar(255) NOT NULL DEFAULT '' COMMENT '高級搜索的字段',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '狀態',
  `engine_type` varchar(25) NOT NULL DEFAULT 'MyISAM' COMMENT '數據庫引擎',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='文檔模型表' AUTO_INCREMENT=14 ;

--
-- 转存表中的数据 `cuiyufang_model`
--

INSERT INTO `cuiyufang_model` (`id`, `name`, `title`, `extend`, `relation`, `need_pk`, `field_sort`, `field_group`, `attribute_list`, `template_list`, `template_add`, `template_edit`, `list_grid`, `list_row`, `search_key`, `search_list`, `create_time`, `update_time`, `status`, `engine_type`) VALUES
(1, 'document', '基礎文檔', 0, '', 1, '{"1":["2","3","5","9","10","11","12","13","14","16","17","19","20"]}', '1:基礎', '', '', '', '', 'id:編號\r\ntitle:標題:[EDIT]&cate_id=[category_id]\r\nupdate_time|time_format:最後更新\r\nstatus_text:狀態\r\nid:操作:article/setstatus?status=-1&ids=[id]|刪除', 0, '', '', 1383891233, 1434092590, 1, 'MyISAM'),
(2, 'article', '文章', 1, '', 1, '{"1":["3","68","67","12","5","24"],"2":["9","13","2","19","10","61","16","17","26","20","14","11","25"]}', '1:基礎,2:擴展', '', '', '', '', 'id:編號\r\ntitle:標題:article/edit?cate_id=[category_id]&id=[id]\r\ncontent:內容', 0, '', '', 1383891243, 1436498837, 1, 'MyISAM'),
(3, 'download', '下載', 1, '', 1, '{"1":["3","28","30","32","2","5","31"],"2":["13","10","9","12","16","17","61","19","11","20","14","29"]}', '1:基礎,2:擴展', '', '', '', '', 'id:編號\r\ntitle:標題', 0, '', '', 1383891252, 1434594377, 1, 'MyISAM'),
(5, 'image', '圖片', 1, '', 1, '{"1":["3","34","35"],"2":["11","12","13","10","5","61","2","9","16","14","19","17","20"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1433476425, 1434594351, 1, 'MyISAM'),
(6, 'product', '產品', 1, '', 1, '{"1":["3","61","37","38","39","5","40"],"2":["9","2","10","11","12","13","14","20","19","17","16"]}', '1:基礎;2:擴展', '', 'lists', '', '', 'id:編號\r\ntitle:標題:[EDIT]&cate_id=[category_id]\r\nupdate_time|time_format:最後更新\r\nstatus_text:狀態\r\n', 10, '', '', 1433476566, 1436511721, 1, 'MyISAM'),
(7, 'contact', '聯繫我們', 1, '', 1, '{"1":["3","41","42","43","44","45"],"2":["9","10","11","5","12","14","13","61","2","17","20","16","19"]}', '1:基礎;2:擴展', '', '', 'contactadd', 'contactedit', 'id:編號', 10, '', '', 1433476818, 1434594324, 1, 'MyISAM'),
(8, 'feedback', '留言板', 0, '', 1, '', '1:基礎', '', '', '', '', '', 10, '', '', 1433477327, 1433477327, 1, 'MyISAM'),
(9, 'type', '產品類型', 1, '', 1, '{"1":["3","52"],"2":["61","13","10","5","2","12","9","20","19","17","16","14","11"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1433477490, 1434593039, 1, 'MyISAM'),
(10, 'attribute', '產品屬性', 1, '', 1, '{"1":["3","53"],"2":["2","5","9","10","11","61","12","13","14","17","20","19","16"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1433477581, 1434592872, 1, 'MyISAM'),
(11, 'logo', 'logo', 1, '', 1, '{"1":["3","54"],"2":["2","5","9","10","61","11","12","13","14","20","19","16","17"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1433477669, 1434594298, 1, 'MyISAM'),
(12, 'config', '網站設置', 1, '', 1, '{"1":["3","55","56","57","58","59","66","65","64"],"2":["9","10","11","12","13","61","14","2","17","5","16","19","20"]}', '1:基礎;2:擴展；', '', '', '', '', 'id:編號', 10, '', '', 1434012376, 1435808888, 1, 'MyISAM'),
(13, 'ad', '廣告', 1, '', 1, '{"1":["3","62"],"\\u64f4\\u5c55":["5","9","19","61","20","14","17","16","13","12","11","2","10"]}', '1:基礎;擴展', '', '', '', '', 'id:編號', 10, '', '', 1435805598, 1435805775, 1, 'MyISAM');

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_picture`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路徑',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '圖片鏈接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1編碼',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `cuiyufang_picture`
--

INSERT INTO `cuiyufang_picture` (`id`, `path`, `url`, `md5`, `sha1`, `status`, `create_time`) VALUES
(1, '/Uploads/Picture/2015-06-05/55713c879de15.jpg', '', 'b483050d4d6801b76ed98b05aea76535', '40dc5da247b3528b6ab78a13f7e972d4298de4a1', 1, 1433484423),
(2, '/Uploads/Picture/2015-06-05/55713d1e77f29.jpg', '', '019f236405621243af8cdbf5ab677b7b', 'a35f7902e8f2e5bdc98f6259e8d9da107d9bb580', 1, 1433484574),
(3, '/Uploads/Picture/2015-06-05/55713d1e98e89.jpg', '', 'a97f8da55426d7987d48ae1e6350797f', '6bbad8ce90c3fa711918b4ce3a9186fd147a2ad2', 1, 1433484574),
(4, '/Uploads/Picture/2015-06-05/55713d9428c7c.jpg', '', '8e41600dfb01e0089db5284a53595c69', 'cb23e6e9a0d723c89da237a4a1693c5411dc0c1e', 1, 1433484692),
(5, '/Uploads/Picture/2015-06-05/55713ddc363cd.jpg', '', '7bf00719bcaa9f5e707ed9b4b283eb85', '4accf70678f1cce7358f824fb13ef261686141f9', 1, 1433484764),
(6, '/Uploads/Picture/2015-06-05/55713de7ee3d5.jpg', '', 'e80b2a90f5f75b4e9fffd2c56a39da36', 'e575f2546a8ccaefea0c3e1f58a47dd6433a6250', 1, 1433484775),
(7, '/Uploads/Picture/2015-06-05/55713dff17b80.jpg', '', '45632ecc4633608e1073dfe30888b1d2', '7e93a843a5688c655d21efb3ead1ce5645d9a069', 1, 1433484799),
(8, '/Uploads/Picture/2015-06-11/557949f0eb58c.png', '', '93e0303c68aefb902fe6f0facc6f1cda', '2c1c22d68426216672425e9a182b1a5a44c16883', 1, 1434012144);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_ucenter_admin`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_ucenter_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理員ID',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理員用戶ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '管理員狀態',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理員表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_ucenter_app`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_ucenter_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '應用ID',
  `title` varchar(30) NOT NULL COMMENT '應用名稱',
  `url` varchar(100) NOT NULL COMMENT '應用URL',
  `ip` char(15) NOT NULL COMMENT '應用IP',
  `auth_key` varchar(100) NOT NULL COMMENT '加密KEY',
  `sys_login` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '同步登陸',
  `allow_ip` varchar(255) NOT NULL COMMENT '允許訪問的IP',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '應用狀態',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='應用表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_ucenter_member`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_ucenter_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶ID',
  `username` char(16) NOT NULL COMMENT '用戶名',
  `password` char(32) NOT NULL COMMENT '密碼',
  `email` char(32) NOT NULL COMMENT '用戶郵箱',
  `mobile` char(15) NOT NULL COMMENT '用戶手機',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '註冊時間',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '註冊IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最後登錄時間',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最後登錄IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) DEFAULT '0' COMMENT '用戶狀態',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用戶表' AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `cuiyufang_ucenter_member`
--

INSERT INTO `cuiyufang_ucenter_member` (`id`, `username`, `password`, `email`, `mobile`, `reg_time`, `reg_ip`, `last_login_time`, `last_login_ip`, `update_time`, `status`) VALUES
(1, 'admin', '1a02861a27e6b4d23bbd572b46d519a8', '1061031727@qq.com', '', 1433475305, 2130706433, 1436508467, 2130706433, 1433475305, 1),
(2, 'user', 'd90267831f2c694d61bb633f890898b6', '1234567@qq.com', '', 1434014713, 2130706433, 0, 0, 1434014713, 1);

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_ucenter_setting`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_ucenter_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '設置ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置類型（1-用戶配置）',
  `value` text NOT NULL COMMENT '配置數據',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='設置表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_url`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '鏈接唯壹標識',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '鏈接地址',
  `short` char(100) NOT NULL DEFAULT '' COMMENT '短網址',
  `status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='鏈接表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cuiyufang_userdata`
--

CREATE TABLE IF NOT EXISTS `cuiyufang_userdata` (
  `uid` int(10) unsigned NOT NULL COMMENT '用戶id',
  `type` tinyint(3) unsigned NOT NULL COMMENT '類型標識',
  `target_id` int(10) unsigned NOT NULL COMMENT '目標id',
  UNIQUE KEY `uid` (`uid`,`type`,`target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
