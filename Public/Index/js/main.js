$(".adv .nav-tabs li").last().css("border", "none");
$(".product_nav li:eq(0)").find("a").eq(0).addClass("active");
$(".product_nav li:eq(1)").find("a").eq(0).addClass("active");


    if($('.search2 a[class="active"]').length>1){
    $(".product_nav li:eq(1)").find("a").eq(0).removeClass("active");
}
     if($('.search1 a[class="active"]').length>1){
    $(".product_nav li:eq(0)").find("a").eq(0).removeClass("active");
}    

$(".product_nav li a").click(function () {
    $(this).addClass("active").siblings().removeClass("active");    
    var types = $('.search1 .active').html();
    var attribute = $('.search2 .active').html();

    $('.type').attr('value',types);
    $('.attribute').attr('value',attribute);
    $('#form1').submit();

});

$(window).scroll(function () {
    $(this).scrollTop() > 1 ? $('.go-top').show() : $('.go-top').hide();
});

$(".go-top").click(function (event) {
    event.preventDefault();
    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 600);
});

/*留言*/
$(".messge_nav").click(function () {
    $(".message_content").slideToggle();
    $(".go-top").toggle();
});


//輪播
var swiperadv = new Swiper('.swiperadv', {
    pagination: '.swiper-paginationadv',
    paginationClickable: true,
    spaceBetween: 30,
    slidesPerView: 1,
    slidesPerColumn: 1,
    nextButton: '.swiper-button-nextadv',
    prevButton: '.swiper-button-prevadv'
});
var swiper0 = new Swiper('.swiper0', {
    pagination: '.swiper-pagination0',
    paginationClickable: true,
    spaceBetween: 30,
    slidesPerView: 1,
    slidesPerColumn: 1,
    nextButton: '.swiper-button-next0',
    prevButton: '.swiper-button-prev0'
});
var swiper1 = new Swiper('.swiper1', {
    pagination: '.swiper-pagination1',
    paginationClickable: true,
    spaceBetween: 30,
    slidesPerView: 1,
    slidesPerColumn: 1,
    nextButton: '.swiper-button-next1',
    prevButton: '.swiper-button-prev1'
});
var swiper2 = new Swiper('.swiper2', {
    pagination: '.swiper-pagination2',
    paginationClickable: true,
    spaceBetween: 30,
    slidesPerView: 1,
    slidesPerColumn: 1,
    nextButton: '.swiper-button-next2',
    prevButton: '.swiper-button-prev2'
});
var swiper3 = new Swiper('.swiper3', {
    pagination: '.swiper-pagination3',
    paginationClickable: true,
    spaceBetween: 30,
    slidesPerView: 1,
    slidesPerColumn: 1,
    nextButton: '.swiper-button-next3',
    prevButton: '.swiper-button-prev3'
});

//產品詳情
$(".prodetail").click(function () {
    var id = $(this).find("div").html();
    $.ajax({
        type: "post",
        url: prodetail,
        data: {'id': id},
        dataType: 'json', // 服务器返回的数据类型 可选XML ,Json jsonp script html  text等                                                                        
        success: function (data) {
            var ndiv = $("#product_li");
            ndiv.empty();
            var newDiv = '<div class="modal-dialog modal-lg">';
            newDiv += '<div class="modal-content">';
            newDiv += '<div class="modal-header product_info_title">';
            newDiv += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>'
                    + data['title'] + '</h3><div class="share_div">分享到：';
           newDiv += '<a href="#" class="share_facebook_f" title="facebook分享"><span class="fa-stack"><i class="fa fa-square fa-stack-2x share_facebook_bg"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></a>';
           newDiv += '<a href="#" class="jiathis_button_weixin" title="wechat分享"><span class="fa-stack"><i class="fa fa-square fa-stack-2x share_weixin_bg"></i><i class="fa fa-wechat fa-stack-1x fa-inverse"></i></span></a></div> </div>';    
           newDiv += '<div class="modal-body row product_info">';
           newDiv += '<div class="col-md-6 product_info_img div_con"><img src="'
                    + data['image'] + '"/></div><div class="col-md-6 text_con"><p>'
                    + data['content']
                    + '</p></div></div></div></div>';
            ndiv.append(newDiv);
        }
    });
});
//圖片居中
     $(window).load(function (){
      $.imgmid();
     });
 $(".product_div").click(function(){
      $.imgmid();
 });
$.extend({imgmid:function(){
      $(".div_con").find("img").each(function(){
        var wd = $(this).parents(".div_con").width(),
              hd = $(this).parents(".div_con").height(),
              wimg_a = $(this).width(),
              himg_a = $(this).height(); //圖片初始寬高

    if (wd/1.2 > hd) {
            $(this).width("100%");
                    himg = $(this).height();
            if (himg > hd) {
                   $(this).css({"margin-top": - Math.abs(himg - hd) / 2});
            } else {
                   $(this).height("100%");  
            }
    } else {
            if (wimg_a > himg_a) {
                $(this).height("100%");
                wimg = $(this).width();
                if (wimg < wd) {
                    $(this).width("100%");
                } else if (wimg > wd) {
                $(this).css({"margin-left": - Math.abs((wimg - wd) / 2)});
               }
            } else if (wimg_a < himg_a) {
                $(this).width("100%");
                himg = $(this).height();
                if (himg < hd) {
                $(this).height("100%");
            } else if (himg > hd) {
                $(this).css({"margin-top": - Math.abs(himg - hd) / 2});
                }
            } else if (wimg_a === himg_a) {
                 $(this).css({"height": "100%", "width": "100%"});
             }
         }
     });           
}});