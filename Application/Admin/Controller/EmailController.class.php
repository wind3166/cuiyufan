<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Admin\Controller;
use Think\Controller;

class EmailController extends AdminController
{
    public function sendemail(){
        $sendto = I('post.email');
        $content = I('post.content');
        if(isset($sendto,$content))
        {
         if($sendto==""||$content=="")
         {
                  $this->error('請填寫好內容'); 
         }
         else
         {   
         $res = D('Email')->sendmail($sendto,'翠玉坊為您轉發了一份郵件',$content);
         if($res)
         {  
          $this->success('轉發成功');
         }
         else
         {
          $this->error('發送失敗');
         }
        }      
        }
        
    }
}