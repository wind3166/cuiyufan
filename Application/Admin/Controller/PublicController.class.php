<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use User\Api\UserApi;

/**
 * 後臺首頁控制器
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */
class PublicController extends \Think\Controller {

    /**
     * 後臺用護登錄
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function login($username = null, $password = null, $verify = null){
        if(IS_POST){
           

            /* 調用UC登錄接口登錄 */
            $User = new UserApi;
            $uid = $User->login($username, $password);
            if(0 < $uid){ //UC登錄成功
                /* 登錄用護 */
                $Member = D('Member');
                if($Member->login($uid)){ //登錄用護
                    //TODO:跳轉到登錄前頁面
                    $this->success('登錄成功！', U('Article/feedback'));
                } else {
                    $this->error($Member->getError());
                }

            } else { //登錄失敗
                switch($uid) {
                    case -1: $error = '用護不存在或被禁用！'; break; //系統級別禁用
                    case -2: $error = '密碼錯誤！'; break;
                    default: $error = '未知錯誤！'; break; // 0-接口慘數錯誤（調試階段使用）
                }
                $this->error($error);
            }
        } else {
            if(is_login()){
                $this->redirect('Article/feedback');
            }else{
                /* 讀取數據庫中的配置 */
                $config	=	S('DB_CONFIG_DATA');
                if(!$config){
                    $config	=	D('Config')->lists();
                    S('DB_CONFIG_DATA',$config);
                }
                C($config); //添加配置
                
                //logo
                $logo=D('Document')->lists(50);
                $logos=D('Document')->detail($logo[0]['id']);
               $this->assign('logo',$logos);
                $this->display();
            }
        }
    }

    /* 退出登錄 */
    public function logout(){
        if(is_login()){
            D('Member')->logout();
            session('[destroy]');
            $this->success('退出成功！', U('login'));
        } else {
            $this->redirect('login');
        }
    }

    public function verify(){
        $verify = new \Think\Verify();
        $verify->entry(1);
    }

}