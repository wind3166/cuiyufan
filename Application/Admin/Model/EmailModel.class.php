<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Admin\Model;
use Think\Model;
use Admin\Model\AuthGroupModel;
class EmailModel extends Model{
      /* 自動驗證規則 */
protected $_validate = array(
   array('email', 'require', '郵箱不能為空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
   array('content', 'require', '內容不能為空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH)
    );
  /* 自動完成規則 */
protected $_auto = array(
    array('email', 'htmlspecialchars', self::MODEL_BOTH, 'function'),
    array('content', 'htmlspecialchars', self::MODEL_BOTH, 'function')
);
public function sendmail($to = '', $subject = '', $body = '', $name = '', $attachment = null) {
        $from_email = C('MAIL_SMTP_USER'); 
        $from_name = C('WEB_SITE');
        $reply_email = '';
        $reply_name = '';
        require_once './Plugin/PHPMailer/PHPMailerAutoload.php';
        $mail = new \PHPMailer;
        $mail->CharSet    = 'UTF-8';
        $mail->isSMTP();
        $mail->SMTPDebug = 0;                       // 關閉SMTP調試功能
                                                    // 1 = errors and messages
                                                    // 2 = messages only
        $mail->SMTPAuth = true;                  // 啟用 SMTP 驗證功能
        $mail->SMTPSecure = '';                 // 使用安全協議 
        $mail->Host = C('MAIL_SMTP_HOST');  // SMTP 服務器
        $mail->Port = C('MAIL_SMTP_PORT');  // SMTP服務器的端口號
        $mail->Username = C('MAIL_SMTP_USER');  // SMTP服務器用戶名
        $mail->Password = C('MAIL_SMTP_PASS');  // SMTP服務器密碼
        $mail->SetFrom($from_email, $from_name);
        $replyEmail = isset($reply_email)? $reply_email : $from_email;
        $replyName = isset($reply_name) ? $reply_name : $from_name;
        if ($to == '') {
            $to = C('MAIL_SMTP_RECEIVE'); //郵件地址為空時，默認使用後臺默認郵件測試地址
        }
        if ($name == '') {
            $name = C('WEB_SITE'); //發送者名稱為空時，默認使用網站名稱
        }
        if ($subject == '') {
            $subject = C('MAIL_SMTP_SUBJECT'); //郵件主題為空時，默認使用網站標題
        }
        if ($body == '') {
            $body = C('MAIL_SMTP_BODY'); //郵件內容為空時，默認使用網站描述
        }
        $mail->AddReplyTo($replyEmail, $replyName);
        $mail->Subject = $subject;
        $mail->MsgHTML($body); //解析
        $mail->AddAddress($to, $name);
        if (is_array($attachment)) { // 添加附件
            foreach ($attachment as $file) {
                is_file($file) && $mail->AddAttachment($file);
            }
        }
        return $mail->Send() ? true : $mail->ErrorInfo; //返回錯誤信息
    }
}