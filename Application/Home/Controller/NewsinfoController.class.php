<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Home\Controller;
use OT\DataDictionary;
/**
 * 最新動態詳情
 *
 * @author Tina
 */
class NewsinfoController extends HomeController {
    
    //logo
    public function _initialize() {
        $logo = D('Document')->getlist('cuiyufang_document_logo',50);        
        $this->assign('logo', $logo[0]);
        
         $conf=D('Document')->getlist('cuiyufang_document_config',51);
        $this->assign('conf',$conf[0]);
    }
    
    //新聞詳情
    public function index(){
        $id=$_GET['id'];
        $list=D('Document')->detail($id);
        $this->assign('list',$list);
        $this->display(news_info);
    }
}
