<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Home\Controller;

use OT\DataDictionary;
/**
 * 留言板控制器
 *
 * @author Tina
 */
class FeedbackController extends HomeController{
  
    
    //留言
    public function index(){
        
        $data = array(
            'name' => htmlspecialchars($_POST['name']),
            'phone' => htmlspecialchars($_POST['phone']),
            'email' => htmlspecialchars($_POST['email']),
            'content' => htmlspecialchars($_POST['content']),
            'time' => time(),
            'status' => 1,
        );
        if ($data['name'] == '' || $data['phone'] == '' || $data['email'] == '' || $data['content'] == '') {
            $this->error('请输入完整信息');
        }
        $id=D('Feedback')->create();        
        if (!$id) {            
            $this->error('郵箱格式錯誤');
        } else {
            if(D('Feedback')->update($data)){
            if (D('Email')->send_mail('', '', '')) {
                $this->success('發送成功，已通知商家');
            } else {
                $this->error('通知商家失敗');
            }
            
            }else{
                $this->error('聯繫失敗');
            }
        }
    }
}
