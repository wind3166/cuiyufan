<?php

// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;

use OT\DataDictionary;

/**
 * 前台首页控制器
 * 主要获取首页聚合数据
 */
class IndexController extends HomeController {

    //系统首页
    public function index() {
        //廣告
        $adv = D('Document')->getlist('cuiyufang_document_ad', 53);
        foreach ($adv[0] as $key => $value) {
            if ($key == 'pics') {
                if (strpos($value, ',')) {
                    $adv[0][$key] = explode(',', $value);
                }
            }
        }
     
        $this->assign('adv', $adv[0]);

        //推薦
        $recom = D('Document')->getlist('cuiyufang_document_product', 45, 1);
        $var = array();
        foreach ($recom as $key => $value) {
            $var[$value['types']][] = $value;
        }        
        foreach ($var as $k => $v) {
          
                  $typess=M('document_type')->where(array('id' => $k))->find();
                  $var1[$k]['type']=$typess["tname"];
              
           $var1[$k]['data']=$v;
        }
        $i=0;
        $var2=array();
       foreach($var1 as $ko=>$vo){

           $var2[$i]=$vo;           
           $i+=1;
       }
        $this->assign('recom', $var2);

        //獨有特色
        $perfact = D('Document')->attrlist('cuiyufang_document_product', 45,1,31);
        $this->assign('perfect',$perfact['data2']);
        $this->display();
    }

    //關於我們
    public function about() {
        $about = D('Document')->getlist('cuiyufang_document_image', 40);
        $pic = explode(',',$about[0]['image']);
        $this->assign('pic',$pic);
        $this->assign('about', $about[0]);
        $this->display();
    }

    //最新動態
    public function news() {
        $list = D('Document')->page(41);
        $this->assign('news', $list['data2']);
        $this->assign('page', $list['data1']);
        $this->display();
    }

    //翡翠產品
    public function products() {
        //類別
        $types = D('Document')->getlist('cuiyufang_document_type', 43);
        $this->assign('types', $types);

        //屬性
        $attribute = D('Document')->getlist('cuiyufang_document_attribute', 44);
        $this->assign('attribute', $attribute);

        //產品列表

        $data = D('Document')->products(45);
        //  dump($data);die;
        $this->assign('product', $data[0]);
        $this->assign('page', $data[1]);
        $this->display();
    }

    //聯繫我們
    public function contact() {
        $contact = D('Document')->getlist('cuiyufang_document_contact', 47);
    
        $this->assign('contact', $contact[0]);
        $this->display();
    }

    //條款細則
    public function rule() {
        $rule = D('Document')->getlist('cuiyufang_document_article', 48);
        $this->assign('rule', $rule[0]);
        $this->display();
    }

    //產品詳情
    public function prodetail() {
        $id = htmlspecialchars($_POST['id']);
        $detail = D('Document')->detail($id);
        $detail['image'] = $value[$k] = __ROOT__ . get_cover($detail['image'], $field = path);
        $this->ajaxReturn($detail, 'JSON');
    }

    //搜索產品
    public function search() {
        //類別
        $types = D('Document')->getlist('cuiyufang_document_type', 43);
        $this->assign('types', $types);

        //屬性
        $attribute = D('Document')->getlist('cuiyufang_document_attribute', 44);
        $this->assign('attribute', $attribute);

        //產品列表
        $title = $_POST['pname'];
        $lists = D('Document')->products(45, $title);
        $this->assign('product', $lists[0]);
        $this->assign('page', $lists[1]);
        $this->display('products');
    }



    //通過種類等搜索商品
    public function lists() {
        //類別
        $types = D('Document')->getlist('cuiyufang_document_type', 43);
        $this->assign('types', $types);

        //屬性
        $attribute = D('Document')->getlist('cuiyufang_document_attribute', 44);
        $this->assign('attribute', $attribute);
        if (IS_POST) {
            $data = array(
                'types' => $_POST['types'],
                'attribute' => $_POST['attribute']
            );
            $typeN = $data['types'];
            $attributeN = $data['attribute'];
            if ($data['types'] == "全部") {
                unset($data['types']);
            } else {
              $type = M('document_type')->where(array('tname' => $data['types']))->find();
               $data['types'] = $type['id'];
            }

            if ($data['attribute'] == "全部") {
                unset($data['attribute']);
            } else {
            $type   = M('document_attribute')->where(array('aname' => $data['attribute']))->find();
               $data['attribute'] = $type['id'];
            }
        } else {
           $data['types']= isset($_GET['types'])?$_GET['types']:'';
           $data['attribute']= isset($_GET['attribute'])?$_GET['attribute']:'';
           if(empty($data['types'])){ unset($data['types']);}else{
               $typeNs=D('Document')->detail($data['types']);
               $typeN=$typeNs['tname'];
           }
           if(empty($data['attribute'])){ unset($data['attribute']);}else{
               $attributeNs=D('Document')->detail($data['types']);
               $attributeN=$attributeNs['aname'];
           }
        }
        
        $lists = D('Document')->productAjax($data);
        $this->assign('product', $lists['data1']);
        $this->assign('page', $lists['data2']);
        $this->assign('typeN',$typeN);
        $this->assign('attributeN',$attributeN);
        $this->display('products');
    }
    //更多
    public function more(){
         //類別
        $types = D('Document')->getlist('cuiyufang_document_type', 43);
        $this->assign('types', $types);
        //屬性
        $attribute = D('Document')->getlist('cuiyufang_document_attribute', 44);
        $this->assign('attribute', $attribute);
        //分類數據
        $perfact = D('Document')->attrlist('cuiyufang_document_product', 45,1,31);
        $this->assign('product',$perfact['data2']);
        $this->assign('page',$perfact['data1']);
        $this->display('products');
    }
}
