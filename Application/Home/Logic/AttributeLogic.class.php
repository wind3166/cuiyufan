<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Home\Logic;
/**
 * 產品屬性模型
 *
 * @author Tina
 */
class AttributeLogic extends BaseLogic{
	/* 自動驗證規則 */
//	protected $_validate = array(
//		array('content', 'require', '內容不能為空！', self::MUST_VALIDATE , 'regex', self::MODEL_BOTH),
//	);

	/* 自動完成規則 */
	protected $_auto = array();

	/**
	 * 新增或添加壹條文章詳情
	 * @param  number $id 文章ID
	 * @return boolean    true-操作成功，false-操作失敗
	 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
	 */
	public function update($id){
		/* 獲取文章數據 */ //TODO: 根據不同用戶獲取允許更改或添加的字段
		$data = $this->create();
		if(!$data){
			return false;
		}
		
		/* 添加或更新數據 */
		if(empty($data['id'])){//新增數據
			$data['id'] = $id;
			$id = $this->add($data);
			if(!$id){
				$this->error = '新增詳細內容失敗！';
				return false;
			}
		} else { //更新數據
			$status = $this->save($data);
			if(false === $status){
				$this->error = '更新詳細內容失敗！';
				return false;
			}
		}

		return true;
	}
}
