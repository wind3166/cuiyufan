<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Home\Model;
use Think\Model;


/**
 *用戶留言板
 *
 * @author Tina
 */
class FeedbackModel extends Model {
  protected $_validate = array(
        array('email', 'email', -5, self::EXISTS_VALIDATE), //邮箱格式不正确
    );
//新增留言
    public function update($data) {
        $id = $this->add($data);              
        return $id;
    }

}
