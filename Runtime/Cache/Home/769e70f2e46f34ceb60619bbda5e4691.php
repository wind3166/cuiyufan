<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="<?php echo ($conf['DESCRIPTION']); ?>">
<meta name="keywords" content="<?php echo ($conf['KEYWORD']); ?>">
<title><?php echo ($foot['WEB_SITE_TITLE']); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/cuiyufan/Public/Index/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/cuiyufan/Public/Index/css/font.css" rel="stylesheet" type="text/css"/>
<link href="/cuiyufan/Public/Index/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/cuiyufan/Public/Index/css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="/cuiyufan/Public/Index/css/swiper.min.css" rel="stylesheet" type="text/css"/>
<link href="/cuiyufan/Public/Index/css/cuiyufan_style.css" rel="stylesheet" type="text/css"/>
 


<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="/cuiyufan/Public/static/bootstrap/js/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="/cuiyufan/Public/static/jquery-1.10.2.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/cuiyufan/Public/static/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/cuiyufan/Public/static/bootstrap/js/bootstrap.min.js"></script>
<!--<![endif]-->
<!-- 页面header钩子，一般用于加载插件CSS文件和代码 -->
<?php echo hook('pageHeader');?>

</head>
<body>
	<!-- 头部 -->
	<!-- 导航条
================================================== -->
<div class="container top">
    <div id="top"></div>
    <div class="logo"><img src="/cuiyufan<?php echo get_cover($logo['logo'],$field = path);?>"/></div>
    <div class="booking"><a href="<?php echo ($foot["Facebook"]); ?>" target="_blank"><i class="fa fa-facebook"></i></a><a href="<?php echo ($foot["Youtube"]); ?>" target="_blank"><i class="fa fa-youtube"></i></a></div>
    <nav class="navbar">
        <div class="container-fluid">
            <a href="#" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><i class="fa fa-bars"></i></a>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php $__NAV__ = M('Channel')->field(true)->where("status=1")->order("sort")->select(); if(is_array($__NAV__)): $i = 0; $__LIST__ = $__NAV__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i; if(($nav["pid"]) == "0"): ?><li>
                            <a href="<?php echo (get_nav_url($nav["url"])); ?>" target="<?php if(($nav["target"]) == "1"): ?>_blank<?php else: ?>_self<?php endif; ?>"><?php echo ($nav["title"]); ?></a>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </ul>

            </div>
            <div class="navbar-right">
                <form class="navbar-form navbar-left" role="search" method="post" action="<?php echo U('Home/Index/search');?>">
                    <div class="form-group">
                        <input name="pname" type="text" class="form-control" placeholder="搜索">
                    </div>
                    <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </nav>
</div>


	<!-- /头部 -->
	
	<!-- 主体 -->
	
<div id="main-container" class="container">
    <div class="row">
        
        <!-- 左侧 nav
        ================================================== -->
            <div class="span3 bs-docs-sidebar">
                
                <ul class="nav nav-list bs-docs-sidenav">
                    <?php echo W('Category/lists', array($category['id'], ACTION_NAME == 'index'));?>
                </ul>
            </div>
        
        

    <!--頂部-->
    <script type="text/javascript">
        $(".navbar-nav li").eq(0).addClass("active");
    </script>


    <!--頂部 結束-->
    <div class="container content">
        <!--廣告輪播-->
        <div class="adv">
            <div class="swiper-container swiperadv">
                <div class="swiper-wrapper">
                    <?php if(is_array($adv[pics])): $i = 0; $__LIST__ = $adv[pics];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$adv): $mod = ($i % 2 );++$i;?><div class="swiper-slide">
                            <a href="#"><img src="/cuiyufan<?php echo (get_cover($adv,'path')); ?>"/></a>
                        </div><?php endforeach; endif; else: echo "" ;endif; ?>
                </div>
                <div class="swiper-pagination swiper-paginationadv"></div>
                <div class="swiper-button-prev swiper-button-prevadv"><i class="fa fa-angle-left"></i></div>
                <div class="swiper-button-next swiper-button-nextadv"><i class="fa fa-angle-right"></i></div>
            </div>

        </div>
        <!--廣告輪播 結束-->
        <!--商品切換-->
        <div class="adv">
            <div class="bs-docs-example">
                <ul id="myTab" class="nav nav-tabs">
                    <?php if(is_array($recom)): $key = 0; $__LIST__ = $recom;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($key % 2 );++$key;?><li class=""><a href="#category<?php echo ($key); ?>" data-toggle="tab"><?php echo ($data['type']); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <script type="text/javascript">
                    $(".nav-tabs li").eq(0).addClass("active");
                </script>                                

                <div id="myTabContent" class="tab-content">                    
                    <?php if(is_array($recom)): $keys = 0; $__LIST__ = $recom;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($keys % 2 );++$keys;?><div class="tab-pane fade " id="category<?php echo ($keys); ?>">                            
                            <div class="swiper-container swiper<?php echo ($keys); ?>">
                                <div class="swiper-wrapper">                       
                                    <?php if(is_array($data['data'])): $i = 0; $__LIST__ = $data['data'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><div class="swiper-slide">
                                            <div class="col-md-6 col-sm-6 col-xs-7 text-right"><img src="/cuiyufan<?php echo (get_cover($value['image'],'path')); ?>"/></div>
                                            <div class="col-md-6 col-sm-6 col-xs-5"><h2><?php echo ($value['title']); ?></h2><p><?php echo ($value['description']); ?></p><a href="<?php echo U('Home/Index/products');?>" class="btn btn_green">查看詳情</a></div>
                                       </div><?php endforeach; endif; else: echo "" ;endif; ?>                                                                    
                                </div>
                                <div class="swiper-pagination swiper-pagination<?php echo ($keys); ?>"></div>
                                <div class="swiper-button-prev swiper-button-prev<?php echo ($keys); ?>"><i class="fa fa-angle-left"></i></div>
                                <div class="swiper-button-next swiper-button-next<?php echo ($keys); ?>"><i class="fa fa-angle-right"></i></div>
                            </div>
                        </div><?php endforeach; endif; else: echo "" ;endif; ?>
                    <script type="text/javascript">
                        $(".tab-pane").eq(0).addClass("active in");
                    </script>
                </div>
            </div>
        </div>
        <!--商品切換 結束-->
        <!--獨有款式-->
        <div class="style_title">獨有款式</div>
        <div class="row adv">
            <div class="col-md-6 style_con">
                <div class="style_con1">
                    <a href="#">  
                        <img src="/cuiyufan<?php echo (get_cover($perfect[0]['image'],'path')); ?>" class="blur"/>
                    </a>
                    <div class="text_con">
                        <h3><?php echo ($perfect[0]['title']); ?></h3>
                        <p><?php echo ($perfect[0]['content']); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6  style_con">
                <div class="row style_con2">
                    <div class="col-md-5 col-sm-5 col-xs-5 text_con">
                        <h3> <?php echo ($perfect[1]['title']); ?></h3>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-7">
                        <a href="#">
                            <img src="/cuiyufan<?php echo (get_cover($perfect[1]['image'],'path')); ?>" class="blur"/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 style_con">
                <div class="row style_con2">
                    <div class="col-md-7 col-sm-7 col-xs-7">
                        <a href="#">
                            <img  src="/cuiyufan<?php echo (get_cover($perfect[2]['image'],'path')); ?>" class="style_con_h blur"/>
                        </a>
                    </div>
                    <div class="text_con col-md-5 col-sm-5 col-xs-5 style_con_h">
                        <h3><?php echo ($perfect[2]['title']); ?></h3>
                    </div>
                </div>
            </div>
        </div>
        <!--獨有款式 結束-->
    </div>

    <block name="footer">
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(window).resize(function(){
            $("#main-container").css("min-height", $(window).height() - 343);
        }).resize();
    })
</script>
	<!-- /主体 -->

	<!-- 底部 -->
	
    <!-- 底部
    ================================================== -->
<!--    <footer class="footer">
      <div class="container">
          <p> 本站由 <strong><a href="http://www.onethink.cn" target="_blank">OneThink</a></strong> 强力驱动</p>
      </div>
    </footer>-->

    <div class="container footer">
      <?php echo ($foot["foot"]); ?>
            <p><?php echo ($foot['copyright']); ?><a href="<?php echo U('Home/Index/rule','','');?>">條款細則</a></p>
        </div>

<div class="messge">
    <div class="messge_nav">聯系我們 CONTACT  US</div>
    <div class="message_content">
        <form action="<?php echo U('Feedback/index','','');?>" method="post" id="form_message">               
            <input class="message_input " type="text" name="name" onfocus="if (value == '姓名 Name') {
                        value = ''
                    }" onblur="if (value == '') {
                                value = '姓名 Name'
                            }" value="姓名 Name" />
            <input class="message_input" type="text" name="phone" onfocus="if (value == '電話 Phone') {
                        value = ''
                    }" onblur="if (value == '') {
                                value = '電話 Phone'
                            }" value="電話 Phone" />
            <input class="message_input" type="text" name="email" onfocus="if (value == '郵箱 Email') {
                        value = ''
                    }" onblur="if (value == '') {
                                value = '郵箱 Email'
                            }" value="郵箱 Email" />
            <textarea class="message_text_area" name="content" onfocus="if (value === '内容 Content') {
                        value = ''
                    }" onblur="if (value === '') {
                                value = '内容 Content'
                            }" value="内容 Content"></textarea>
            
            <input class="message_button" type="submit" value="發送 Send" />
        </form>
    </div>
</div>
        <a class="go-top" href="#top"><i class="fa fa-angle-up"></i></a>
        <script src="/cuiyufan/Public/Index/js/jquery.min.js" type="text/javascript"></script>
        <script src="/cuiyufan/Public/Index/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/cuiyufan/Public/Index/js/jquery.prettyPhoto.js" type="text/javascript"></script>
        <script src="/cuiyufan/Public/Index/js/swiper.min.js" type="text/javascript"></script>
        <script src="/cuiyufan/Public/Index/js/main.js" type="text/javascript"></script>
    
<script type="text/javascript">
(function(){
	var ThinkPHP = window.Think = {
		"ROOT"   : "/cuiyufan", //当前网站地址
		"APP"    : "/cuiyufan", //当前项目地址
		"PUBLIC" : "/cuiyufan/Public", //项目公共目录地址
		"DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
		"MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
		"VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
	}
})();
</script>
 <!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	<?php echo ($foot['ANALYTICS']); ?>
</div>

	<!-- /底部 -->
</body>
</html>